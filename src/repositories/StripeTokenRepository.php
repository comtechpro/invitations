<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class StripeTokenRepository extends Model
{
    public $newBaseQueryBuilder;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->newBaseQueryBuilder = $this->newBaseQueryBuilder()->from('stripe_tokens');
    }

    public static function hydrateStripeTokenModel(array $data)
    {
        $stripeTokenRepository = new self();
        $stripeToken = [];
        $stripeToken['stripe_token'] = $data['stripeToken'];
        $stripeToken['stripe_token_type'] = $data['stripeTokenType'];
        $stripeToken['stripe_email'] = $data['stripeEmail'];

        try {
            $stripeTokenRepository->newBaseQueryBuilder->from('stripe_tokens')->insert($stripeToken);
        } catch (Exception $e) {
            print_r($e);
            die();
        }

        return $data;
    }
}
