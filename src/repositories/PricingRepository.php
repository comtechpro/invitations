<?php

namespace App\Repositories;

use App\Controllers\HandlerController;
use Illuminate\Database\Eloquent\Model;

class PricingRepository extends Model
{
    public $newBaseQueryBuilder;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->newBaseQueryBuilder = $this->newBaseQueryBuilder()->from('prices');
    }

    public static function fetchPriceByCountry($country)
    {
        $priceRepository = new self();
        $price = $priceRepository->newBaseQueryBuilder->from('prices')->where('country', '=', $country)->first();

        if (!$price) {
            HandlerController::errorPage(HandlerController::PRICE_NOT_FOUND, $country);
        }
        return $price->amount;
    }
}
