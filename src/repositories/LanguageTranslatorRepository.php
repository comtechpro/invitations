<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class LanguageTranslatorRepository extends Model
{
    public $newBaseQueryBuilder;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->newBaseQueryBuilder = $this->newBaseQueryBuilder()->from('language_translator');
    }

    /**
     * @param $origin_language_id
     * @return array
     * @throws \Exception
     */
    public static function fetchLanguageTranslators($origin_language_id)
    {
        $languageTranslatorRepository = new self();
        $translators = $languageTranslatorRepository
            ->newQuery()
            ->from('translators')->join('language_translator', 'language_translator.translator_id', '=', 'translators.id', 'left')
            ->where('language_id', '=', (int) $origin_language_id)
            ->get(['translator_id','translators.email']);

        return $translators->toArray();
    }
}
