<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;

class StripeChargeRepository extends Model
{
    public $newBaseQueryBuilder;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->newBaseQueryBuilder = $this->newBaseQueryBuilder()->from('stripe_tokens');
    }
    public static function hydrateStripeChargeModel(array $charge)
    {
        $data = $charge[0];
        $stripeChargeRepository = new self();
        $stripeCharge = [];
        $stripeCharge['id'] = $data['id'];
        $stripeCharge['object'] = $data['object'];
        $stripeCharge['amount'] = $data['amount'];
        $stripeCharge['balance_transaction'] = $data['balance_transaction'];
        $stripeCharge['currency'] = $data['currency'];
        $stripeCharge['description'] = $data['description'];
        $stripeCharge['paid'] = $data['paid'];
        $stripeCharge['status'] = $data['status'];

        try {
            $stripeChargeRepository->newBaseQueryBuilder->from('stripe_charges')->insert($stripeCharge);
        } catch (QueryException $exception) {
            die(var_dump($exception->getMessage()));
        } catch (Stripe\Error\InvalidRequest $exception) {
            die(var_dump($exception->getMessage()));
        }

        return $data;
    }
}
