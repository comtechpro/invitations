<?php

namespace App\Repositories;

use App\Models\NameTranslation;
use App\Services\NameTranslationHelper;
use Illuminate\Database\Eloquent\Model;

class NameTranslationRepository extends Model
{
    public $newBaseQueryBuilder;
    protected $table = 'name_translations';

    /**
     * NameTranslationRepository constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->newBaseQueryBuilder = $this->newBaseQueryBuilder();
    }

    public function updateNameTranslation($token, $translatedName, $status = NameTranslation::PENDING, $translator = null)
    {
        return $this->newBaseQueryBuilder
            ->where('token', '=', $token)
            ->update(['status' => $status, 'translator_id' => $translator, 'translated_name' => $translatedName]);
    }

    /**
     * @param $originLanguageId
     * @param $originName
     * @param $translatedLanguageId
     * @throws \Exception
     */
    public function createNewNameTranslation($originLanguageId, $originName, $translatedLanguageId)
    {
        if ($this->getPendingTranslatedName($originName, $translatedLanguageId) === 0) {
            try {
                $newNameTranslation = $this->hydrateNameTranslationModel($originLanguageId, $originName, $translatedLanguageId);
            } catch (\Exception $exception) {
                print_r($exception->getMessage());
                die();
            }

            NameTranslationHelper::emailLanguageTranslators($newNameTranslation);
        }
    }

    /**
     * @param $originLanguageId
     * @param $originName
     * @param $translatedLanguageId
     * @param null $translatedName
     * @param null $translatorId
     * @return array|bool
     */
    public function hydrateNameTranslationModel($originLanguageId, $originName, $translatedLanguageId, $translatedName = null, $translatorId = null)
    {
        $newNameTranslationModel = [];
        $newNameTranslationModel['origin_language_id'] = $originLanguageId;
        $newNameTranslationModel['origin_name'] = $originName;
        $newNameTranslationModel['translated_language_id'] = $translatedLanguageId;
        $newNameTranslationModel['translated_name'] = $translatedName;
        $newNameTranslationModel['translator_id'] = $translatorId;
        $newNameTranslationModel['token'] = bin2hex(random_bytes(16));

        try {
            $this->newBaseQueryBuilder->from('name_translations')->insert($newNameTranslationModel);
            return $newNameTranslationModel;
        } catch (\Exception $exception) {
            print_r($exception->getMessage());
            die();
        }
    }

    /**
     * @param $originName
     * @return Model|null|static
     */
    public function findByOriginName($originName)
    {
        return  $this->newBaseQueryBuilder->from('name_translations')->where('origin_name', '=', $originName)->first();
    }

    /**
     * @param $token
     * @return Model|null|static
     */
    public function findBytoken($token)
    {
        return  $this->newBaseQueryBuilder->from('name_translations')->where('token', '=', $token)->first();
    }

    public function findPendingNametranslationBytoken($token)
    {
        return  $this->newBaseQueryBuilder()
            ->from('name_translations')
            ->where('token', '=', $token)
            ->where('status', '=', NameTranslation::PENDING)
            ->first();
    }



    /**
     * @param $originName
     * @param $translatedLanguageId
     * @return int
     */
    public function getPendingTranslatedName($originName, $translatedLanguageId)
    {
        return $this->newQuery()
            ->where('translated_language_id', '=', $translatedLanguageId)
            ->where('origin_name', $originName)
            ->where('status', '=', 0)
            ->count();
    }

    /**
     * @param $originLanguageId
     * @param $originName
     * @param $translatedLanguageId
     * @return Model|null|static
     * @throws \Exception
     */
    public function getTranslatedName($originLanguageId, $originName, $translatedLanguageId)
    {
        $translatedName = $this->newQuery()
            ->from('name_translations')
            ->where('translated_language_id', '=', $translatedLanguageId)
            ->where('origin_name', '=', $originName)
            ->where('status', '=', NameTranslation::TRANSLATED)
            ->first();

        if ($translatedName === null) {
            $this->createNewNameTranslation($originLanguageId, $originName, $translatedLanguageId);
            return $translatedName;
        }

        return $translatedName->translated_name;
    }
}
