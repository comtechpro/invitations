<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class TranslatorRepository extends Model
{
    public $newBaseQueryBuilder;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->newBaseQueryBuilder = $this->newBaseQueryBuilder()->from('translators');
    }

    public static function fetchTranslatorEmailById($translatorId)
    {
        $translatorRepository = new self();

        return $translatorRepository->newBaseQueryBuilder->from('translators')->where('id', '=', $translatorId->translator_id)->get();
    }
}
