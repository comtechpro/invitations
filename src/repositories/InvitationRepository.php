<?php

declare(strict_types = 1);

namespace App\Repositories;

use App\Models\Invitation;
use Dompdf\Exception;
use Illuminate\Database\Eloquent\Model;

/**
 * Class InvitationRepository
 * @package App\Repositories
 */
class InvitationRepository extends Model
{
    public $newBaseQueryBuilder;
    public $nameTranslationRepository;

    public function __construct(array $attributes = [], NameTranslationRepository $nameTranslationRepository)
    {
        parent::__construct($attributes);
        $this->newBaseQueryBuilder = $this->newBaseQueryBuilder();
        $this->nameTranslationRepository = $nameTranslationRepository;
    }

    /**
     * @param array $data
     * @return int
     * @throws \Exception
     */
    public function hydrateInvitationModel(array $data)
    {
//        die(var_dump($data['middle_name'] === ''));
        $invitation = [];
        $invitation['email'] = $data['email'];
        $invitation['origin_first_name'] = $data['first_name'];
        $invitation['translated_first_name'] = $this->nameTranslationRepository->getTranslatedName(
            $data['origin_language_id'],
            $data['first_name'],
            $data['translated_language_id']
        );
        $invitation['origin_last_name'] = $data['last_name'];
        $invitation['translated_last_name']  = $this->nameTranslationRepository->getTranslatedName(
            $data['origin_language_id'],
            $data['last_name'],
            $data['translated_language_id']
        );
        $invitation['origin_middle_name'] = '';
        $invitation['translated_middle_name'] = '';
        if ($data['middle_name'] !== '') {
            $invitation['origin_middle_name'] = $data['middle_name'];
            $invitation['translated_middle_name']  = $this->nameTranslationRepository->getTranslatedName(
                $data['origin_language_id'],
                $data['middle_name'],
                $data['translated_language_id']
            );
        }

        $invitation['date_of_birth'] = $data['date_of_birth'];
        $invitation['citizenship'] = $data['citizenship'];
        $invitation['payment_status'] = $data['payment_status'];
        $names = $data['middle_name'] === ''? [$data['first_name'], $data['last_name'] ]: [$data['first_name'], $data['middle_name'], $data['last_name']];
        $invitation['translation_status'] = $this->getTranslationStatus(
            $data['origin_language_id'],
            $names,
            $data['translated_language_id']
            );
        $invitation['send_status'] = Invitation::SEND_STATUS_UNSENT;
        $invitation['visa_type'] = $data['visa_type'];
        $invitation['origin_language_id'] = $data['origin_language_id'];
        $invitation['translated_language_id'] = $data['translated_language_id'];
        $invitation['entries'] = $data['entries'];
        $invitation['gender'] = $data['gender'];
        $invitation['date_entry'] = $data['date_entry'];
        $invitation['date_exit'] = $data['date_exit'];
        $invitation['pers_city'] = $data['pers_city'];

        try {
            return $this->newBaseQueryBuilder->from('invitations')->insertGetId($invitation);
        } catch (Exception $e) {
            print_r($e);
            die();
        }
    }

    public function getTranslationStatusById($id)
    {
        return  $this->newBaseQueryBuilder
            ->from('invitations')
            ->where('id', '=', $id)
            ->get('translation_status');
    }

    /**
     * @param $originLanguageId
     * @param $names
     * @param $translatedLanguageId
     * @return int
     * @throws \Exception
     */
    public function getTranslationStatus($originLanguageId, $names, $translatedLanguageId)
    {
        $res = Invitation::TRANSLATION_STATUS_TRANSLATED;

        foreach ($names as $name) {
            $translatedName = $this->nameTranslationRepository->getTranslatedName($originLanguageId, $name, $translatedLanguageId);
            if ($translatedName === null) {
                $res = Invitation::TRANSLATION_STATUS_PENDING;
            }
        }

        return $res;
    }


//    public function createNameTranslation($originLanguageId, $originName, $translatedLanguageId)
//    {
//        try {
//            return $this->nameTranslationRepository->createNewNameTranslation([
//                'origin_language_id' => $originLanguageId,
//                'origin_name' => $originName,
//                'translated_language_id' => $translatedLanguageId,
//                'translated_name' => null,
//                'translator_id' => null,
//                'status' => NameTranslation::TRANSLATED
//            ]);
//        } catch (\Exception $e) {
//            print_r($e->getMessage());
//            die();
//        }
//    }

    /**
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public function findInvitationById($id)
    {
        return  $this->newBaseQueryBuilder
            ->from('invitations')
            ->where('id', '=', $id)
            ->get();
    }

    public function updateInvitation($id, $parameters)
    {
        $invitation = $this->findInvitationById($id);
        foreach ($parameters as $key => $value) {
            $invitation[$key] = $value;
        }
        $this->newBaseQueryBuilder->from('invitations')->update($invitation->toArray());

        return $invitation;
    }

    public function getUnsentTranslatedInvitations()
    {
       return $this->newBaseQueryBuilder->from('invitations')
            ->where('payment_status', '=', Invitation::PAYMENT_STATUS_PAID)
            ->where('translation_status', '=', Invitation::TRANSLATION_STATUS_TRANSLATED)
            ->where('send_status', '=', Invitation::SEND_STATUS_UNSENT)
            ->get();
    }

    /**
     * @param $data
     * @param $invitation
     * @throws \Exception
     */
//    public function checkInvitationTranslationStatus($data, $invitation)
//    {
//die('1');
//        if($invitation['translated_first_name'] === null)
//        {die('2');
//            if($this->nameTranslationRepository->checkPendingNameTranslation($data['origin_language_id'], $invitation['origin_first_name'], $data['translated_language_id']) === null){
//                die('3');
//                $this->nameTranslationRepository->createNewNameTranslation([$data['origin_language_id'], $invitation['origin_first_name'], $data['translated_language_id']]);
//            }
//        }
//
//        if($invitation['translated_last_name'] === null)
//        {
//            if($this->nameTranslationRepository->checkPendingNameTranslation($data['origin_language_id'], $invitation['origin_last_name'], $data['translated_language_id']) === null){
//                $this->nameTranslationRepository->createNewNameTranslation([$data['origin_language_id'], $invitation['origin_last_name'], $data['translated_language_id']]);
//            }
//
//        }
//
//        if($invitation['translated_middle_name'] === null)
//        {
//            if($this->nameTranslationRepository->checkPendingNameTranslation($data['origin_language_id'], $invitation['origin_middle_name'], $data['translated_language_id']) === null){
//                $this->nameTranslationRepository->createNewNameTranslation([$data['origin_language_id'], $invitation['origin_middle_name'], $data['translated_language_id']]);
//            }
//        }
//    }
}
