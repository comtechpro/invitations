<?php

namespace App\Controllers;

use App\Models\NameTranslation;
use App\Repositories\InvitationRepository;
use App\Models\Invitation;

class InvitationController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function preview()
    {
        $view = "invitation.preview";
        echo $this->blade
            ->view()
            ->make($view, $this->data)->render();
    }

    /**
     * @param string $language
     * @throws \Exception
     */
    public function store($language = 'en')
    {
        if (!$_POST) {
            HandlerController::errorPage(HandlerController::PAGE_NOT_FOUND, $language);
        }

//        post validation - each invitation data must contain all parameters
        $invitationsData = $_POST;
        $invitationIds = '';
//        multiple invitations
//        if(isset($invitationsData['numberOfForms'])){
//            foreach($invitationsData as $invitationData){
//                $data = [];
//                $data['email'] = $_POST['email'];
//                $data['first_name'] = $_POST['first_name'];
//                $data['last_name'] = $_POST['last_name'];
//                $data['middle_name'] = $_POST['middle_name'];
//                $data['citizenship'] = $_POST['citizenship'];
//                $data['date_of_birth'] = $_POST['date_of_birth'];
//                $data['entries'] = $_POST['entries'];
//                $data['gender'] = $_POST['gender'];
//                $data['date_entry'] = $_POST['dateEntry'];
//                $data['date_exit'] = $_POST['dateExit'];
//                $data['pers_city'] = $_POST['pers_city'];
//                $data['origin_language_id'] = NameTranslation::GERMAN_LANGUAGE;
//                $data['translated_language_id'] = NameTranslation::RUSSIAN_LANGUAGE;
//                $data['payment_status'] = Invitation::PAYMENT_STATUS_UNPAID;
//                $data['visa_type'] = Invitation::VISA_TYPE_TOURIST;
//                $invitation = $this->invitationRepository->hydrateInvitationModel($data);
//                $invitationIds .= $invitation['id'] . '-';
//            }
//            substr($invitationIds, 0, -1);
//        }else{
        $data = [];
        $data['email'] = $_POST['email'];
        $data['first_name'] = $_POST['first_name'];
        $data['last_name'] = $_POST['last_name'];
        $data['middle_name'] = $_POST['middle_name'];
        $data['citizenship'] = $_POST['citizenship'];
        $data['date_of_birth'] = $_POST['date_of_birth'];
        $data['entries'] = $_POST['entries'];
        $data['gender'] = $_POST['gender'];
        $data['date_entry'] = $_POST['dateEntry'];
        $data['date_exit'] = $_POST['dateExit'];
        $data['pers_city'] = $_POST['pers_city'];
        $data['origin_language_id'] = NameTranslation::GERMAN_LANGUAGE;
        $data['translated_language_id'] = NameTranslation::RUSSIAN_LANGUAGE;
        $data['payment_status'] = Invitation::PAYMENT_STATUS_UNPAID;
        $data['visa_type'] = Invitation::VISA_TYPE_TOURIST;
        $invitationIds = $this->invitationRepository->hydrateInvitationModel($data);
//        }

        $this->proceedToStripe($invitationIds);

//        InvitationRepository::checkInvitationTranslationStatus($data, $invitation);
    }



    public function proceedToStripe($invitationIds)
    {
        $view = "stripe.index";
        $this->data['stripeToken'] = 'pk_test_6pRNASCoBOKtIshFeQd4XMUh';
        $this->data['amount'] = 55000;
        $this->data['currency'] = '$';
        $this->data['action'] = 'stripe/processing/invitation/' . $invitationIds;
        echo $this->blade
            ->view()
            ->make($view, $this->data)->render();
    }
}
