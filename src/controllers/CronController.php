<?php


namespace App\Controllers;

use App\Repositories\InvitationRepository;
use App\Repositories\NameTranslationRepository;
use App\Services\EmailService;
use App\Services\PdfGenerator;

class CronController
{
    public function emailTranslatedInvitations()
    {
        $pdf = new PdfGenerator();
        $invitationRepository = new InvitationRepository([], new NameTranslationRepository());
        $invitations = $invitationRepository->getUnsentTranslatedInvitations();
        foreach($invitations as $invitation){
            $pdfInvitation = $pdf->generateInvitation($invitation);
            $content = '';
            $this->sendInvitationPdf($invitations->email, 'PDF Invitation', $content, $pdfInvitation);
        }
    }

    public function sendInvitationPdf($to, $title, $content, $pdfInvitation)
    {
        return true;
//        EmailService::sendEmail();
    }

}




