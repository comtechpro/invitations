<?php


namespace App\Controllers;

use App\Repositories\StripeChargeRepository;
use Stripe\Charge;
use Stripe\Stripe;
use App\Repositories\StripeTokenRepository;
use Illuminate\Routing\Controller;

class SubmitController extends Controller
{
    public function processing()
    {
        if ($_POST) {
            $stripeToken = $_POST['stripeToken'];
            $stripeTokenType = $_POST['stripeTokenType'];
            $stripeEmail = $_POST['stripeEmail'];

            $stripeTokenModel = StripeTokenRepository::create([
                'stripeToken' => $stripeToken,
                'stripeTokenType' => $stripeTokenType,
                'stripeEmail' => $stripeEmail
            ]);

            Stripe::setLogger(Stripe::getLogger());
            Stripe::setApiKey('sk_test_BQokikJOvBiI2HlWgH4olfQ2');

            // Set your secret key: remember to change this to your live secret key in production
            // See your keys here: https://dashboard.stripe.com/account/apikeys
            // Token is created using Checkout or Elements!
            // Get the payment token ID submitted by the form:


            // Charge the user's card:
            $stripeCharge = Charge::create(array(
                "amount" => 1000,
                "currency" => "usd",
                "description" => "Example charge",
                "source" => $stripeToken,
            ));

            $stripeChargeModel = StripeChargeRepository::create([

                'id' => $stripeCharge->id,
                'object' => $stripeCharge->object,
                'amount' => $stripeCharge->amount,
                'balance_transaction' => $stripeCharge->balance_transaction,
                'currency' => $stripeCharge->currency,
                'description' => $stripeCharge->description,
                'paid' => $stripeCharge->paid,
                'status' => $stripeCharge->status,
            ]);
        }
    }
}
