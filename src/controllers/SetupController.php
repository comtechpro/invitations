<?php

namespace App\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Database\Capsule\Manager as Capsule;
use App\Models\NameTranslation;

class SetupController extends Controller
{
    public function migrate($token)
    {
//       list of models: User Translation Translator  LanguageTranslator

        //Translators
        if (!Capsule::schema()->hasTable('translators')) {
            Capsule::schema()->create('translators', function ($table) {
                $table->increments('id');
                $table->string('email')->unique();
                $table->string('first_name');
                $table->string('last_name');
                $table->timestamp('created')->useCurrent();
            });
        }

        //LanguageTranslator
        if (!Capsule::schema()->hasTable('language_translator')) {
            Capsule::schema()->create('language_translator', function ($table) {
                $table->increments('id');
                $table->integer('translator_id');
                $table->integer('language_id');
                $table->timestamp('created')->useCurrent();
            });
        }

        // Invitation
        if (!Capsule::schema()->hasTable('invitations')) {
            Capsule::schema()->create('invitations', function ($table) {
                $table->increments('id');
                $table->string('email');
                $table->string('origin_first_name');
                $table->string('translated_first_name')->nullable();
                $table->string('origin_last_name');
                $table->string('translated_last_name')->nullable();
                $table->string('origin_middle_name');
                $table->string('translated_middle_name')->nullable();
                $table->string('date_of_birth');
                $table->string('citizenship');
                $table->integer('payment_status');
                $table->integer('translation_status');
                $table->integer('send_status');
                $table->string('visa_type');
                $table->string('entries');
                $table->string('gender');
                $table->string('date_entry');
                $table->string('date_exit');
                $table->string('pers_city');
                $table->integer('origin_language_id')->nullable();
                $table->integer('translated_language_id')->nullable();
                $table->timestamp('created')->useCurrent();
            });
        }

        //NameTranslation
        if (!Capsule::schema()->hasTable('name_translations')) {
            Capsule::schema()->create('name_translations', function ($table) {
                $table->increments('id');
                $table->integer('origin_language_id');
                $table->string('origin_name');
                $table->integer('translated_language_id');
                $table->string('translated_name')->nullable();
                $table->integer('translator_id')->nullable();
                ;
                $table->integer('status')->default(NameTranslation::PENDING);
                $table->string('token')->default('');
                $table->timestamp('created')->useCurrent();
            });
        }

        //StripeTokens
        if (!Capsule::schema()->hasTable('stripe_tokens')) {
            Capsule::schema()->create('stripe_tokens', function ($table) {
                $table->increments('id');
                $table->string('stripeToken');
                $table->string('stripeTokenType');
                $table->string('stripeEmail');
                $table->timestamp('created')->useCurrent();
            });
        }

        if (!Capsule::schema()->hasTable('stripe_charges')) {
            Capsule::schema()->create('stripe_charges', function ($table) {
                $table->string('id');
                $table->string('object');
                $table->integer('amount');
                $table->string('balance_transaction');
                $table->string('currency');
                $table->text('description');
                $table->tinyInteger('paid');
                $table->string('status');
                $table->timestamp('created')->useCurrent();
            });
        }

        if (!Capsule::schema()->hasTable('prices')) {
            Capsule::schema()->create('prices', function ($table) {
                $table->string('id');
                $table->string('country');
                $table->integer('amount');
                $table->string('currency');
                $table->text('description');
                $table->timestamp('created')->useCurrent();
            });
        }

//        translators
    }
}
