<?php

namespace App\Controllers;

use App\Repositories\PricingRepository;
use App\Services\EmailService;

class HomeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($language)
    {
        if (in_array($language, $this->languages)) {
            $this->data['language'] = $language;
        }

        $view = "home." . $this->data['language'] . ".index";
        $this->data['price'] = PricingRepository::fetchPriceByCountry($this->country);
        $this->data['cents'] = 55;
        $this->data['currency'] = '$';

        echo $this->blade
            ->view()
            ->make($view, $this->data)->render();
    }
}
