<?php

namespace App\Controllers;

use App\Models\Invitation;
use App\Repositories\StripeChargeRepository;
use App\Repositories\StripeTokenRepository;
use Stripe\Charge;
use Stripe\Stripe;

class StripeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \Exception
     */
    public function processing($id)
    {
        $invitation = $this->invitationRepository->findInvitationById($id);
        try {
            StripeTokenRepository::hydrateStripeTokenModel($_POST);
        } catch (\Exception $exception) {
            die(var_dump($exception));
        }

        if ($this->processingCharge()) {
            $invitation = $this->invitationRepository->updateInvitation($id, ['payment_status', Invitation::PAYMENT_STATUS_PAID]);
        }

        if ($invitation->translation_status == Invitation::TRANSLATION_STATUS_TRANSLATED) {
            $this->generate($invitation);
        }

        return HandlerController::successPage();
    }

    public function processingCharge()
    {
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        Stripe::setApiKey($this->stripeApiKey);

        // Token is created using Checkout or Elements!
        // Get the payment token ID submitted by the form:
        $token = $_POST['stripeToken'];
        $charge = Charge::create(array(
            "amount" => 1000,
            "currency" => "usd",
            "description" => "Viselio Tourist Invitation Charge",
            "source" => $token,
        ));

        try {
            $res = StripeChargeRepository::hydrateStripeChargeModel([$charge]);
        } catch (\Exception $exception) {
            die(var_dump($exception));
        }

        return $res;
    }

    /**
     * @param $invitation
     * @throws \Exception
     */
//    public function generate($invitation)
//    {
//
//        $pdf = new Generator(new Dompdf());
//        $status = $this->invitationRepository->getTranslationStatus(
//            1,
//            [
//                $invitation['translated_first_name'],
//                $invitation['translated_last_name'],
//                $invitation['translated_middle_name']
//            ],
//            2);
//        if($status){
//            try {
//                $pdf->generate($invitation, true, true);
//                $this->invitationRepository->updateInvitation('payment_status', Invitation::PAYMENT_STATUS_PAID);
////                    EmailService::sendEmail('minja.mediain@gmail.com', 'test title', 'bkjkhjhkjhkhkjhkj');
//            }catch(Exception $exception){
//                die(var_dump($exception->getMessage()));
//            }
//            return HandlerController::successPage();
//        }
//    }
}
