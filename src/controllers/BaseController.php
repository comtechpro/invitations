<?php

namespace App\Controllers;

use App\Repositories\InvitationRepository;
use App\Repositories\NameTranslationRepository;
use Illuminate\Routing\Controller;
use App\Blade;

class BaseController extends Controller
{
    protected $blade;
    protected $languages;
    public $data;
    public $redirect;
    public $country;
    public $stripeApiKey;
    public $invitationRepository;

    public function __construct()
    {
        $this->country = 'en';
        $this->stripeApiKey = 'sk_test_BQokikJOvBiI2HlWgH4olfQ2';
        $views = __DIR__ . '/../views';
        $cache = __DIR__ . '/../cache';
        $this->blade = new Blade($views, $cache);
        $this->data['language'] = 'en';
        $this->data['public_path'] = 'http://etraveldocs.de/';
        $this->invitationRepository = new InvitationRepository([], new NameTranslationRepository());

        $this->languages = [
            'en',
            'de',
            'da',
            'fi',
            'fr',
            'it',
            'nl',
            'no',
            'pl',
            'es',
            'sw',
        ];
    }


}
