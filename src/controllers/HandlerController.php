<?php

namespace App\Controllers;

class HandlerController extends BaseController
{
    public const PAGE_NOT_FOUND = 1;
    public const STRIPE_ERROR = 2;
    public const PRICE_NOT_FOUND = 3;
    public const NAME_TRANSLATION_NOT_FOUND = 4;
    public const NAME_ALREADY_TRANSLATED = 5;
    public const TRANSLATION_ERROR = 6;

    public function __construct()
    {
        parent::__construct();
    }

    public static function successPage($message = '', $lang = 'en')
    {
        $handler = new self();
        $view = "success.index";
//        $handler->data['message'] = $handler->getErrorContent($errorCode, $lang);

        echo $handler->blade
            ->view()
            ->make($view, $handler->data)->render();
    }


    public static function errorPage($errorCode, $lang = 'en')
    {
        $handler = new self();
        $view = "error.index";
        $handler->data['message'] = $handler->getErrorContent($errorCode, $lang);

        echo $handler->blade
            ->view()
            ->make($view, $handler->data)->render();
    }

    private function getErrorContent($errorCode, $lang)
    {
        switch ($errorCode) {

            case self::PAGE_NOT_FOUND:
                $content['en'] = 'Page Not Found';
                $content['de'] = 'Page Not German';
                $content['fr'] = 'Page Not French';

                return $content[$lang];

            case self::STRIPE_ERROR:
                $content['en'] = 'Stripe Error English';
                $content['de'] = 'Stripe Error German';
                $content['fr'] = 'Stripe Error French';

                return $content[$lang];

            case self::NAME_TRANSLATION_NOT_FOUND:
                $content['en'] = 'Name Translation Not Found Error English';
                $content['de'] = 'Name Translation Not Found Error German';
                $content['fr'] = 'Name Translation Not Found Error French';

                return $content[$lang];
            case self::TRANSLATION_ERROR:
                $content['en'] = 'Name Translation Error English';
                $content['de'] = 'Name Translation Error German';
                $content['fr'] = 'Name Translation Error French';

                return $content[$lang];

                case self::NAME_ALREADY_TRANSLATED:
                $content['en'] = 'Name Already Translated';
                $content['de'] = 'Name Already Translated German';
                $content['fr'] = 'Name Already Translated French';

                return $content[$lang];

        }
    }
}
