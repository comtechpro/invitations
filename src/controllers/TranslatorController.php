<?php

namespace App\Controllers;

use App\Models\NameTranslation;

class TranslatorController extends BaseController
{
    public function index($id, $token)
    {
        $view = "translator.index";
        $nameTranslation = $this->invitationRepository->nameTranslationRepository->findBytoken($token);
        if (!$nameTranslation) {
            return HandlerController::errorPage(HandlerController::NAME_TRANSLATION_NOT_FOUND);
        }

        $this->data['origin_name'] = $nameTranslation->origin_name;
        $this->data['token'] = $token;
        $this->data['id'] = $id;

        echo $this->blade->view()->make($view, $this->data)->render();
    }

    public function translate($translatorId, $token)
    {
        $view = "translator.translate";
        $this->data['lang'] = 'en';
        $nameTranslation =  $this->invitationRepository->nameTranslationRepository->findPendingNametranslationBytoken($token);
        if (!$nameTranslation) {
            HandlerController::errorPage(HandlerController::NAME_ALREADY_TRANSLATED);
        }

        $this->data['originName'] = $nameTranslation->origin_name;
        $this->data['translatorId'] = $translatorId;
        $this->data['token'] = $token;

        echo $this->blade->view()->make($view, $this->data)->render();
    }

    public function store()
    {
        $token = $_POST['token'];
        $translatorId = $_POST['translatorId'];
        $translatedName = $_POST['translatedName'];
        $nameTranslation =  $this->invitationRepository->nameTranslationRepository->findBytoken($token);
        if (!$nameTranslation) {
            HandlerController::errorPage(HandlerController::NAME_TRANSLATION_NOT_FOUND);
        }

        if ($nameTranslation->status === NameTranslation::TRANSLATED) {
            HandlerController::errorPage(HandlerController::NAME_ALREADY_TRANSLATED);
        }

        $nameTranslation->translated_name = $_POST['translatedName'];
        $nameTranslation->translator_id = $_POST['translatorId'];
        $nameTranslation->status = NameTranslation::TRANSLATED;

        if (!$this->invitationRepository->nameTranslationRepository->updateNameTranslation($token, $translatedName, NameTranslation::TRANSLATED, $translatorId)) {
            HandlerController::errorPage(HandlerController::TRANSLATION_ERROR);
        }

        HandlerController::successPage('Thx for translation!!!');
    }
}
