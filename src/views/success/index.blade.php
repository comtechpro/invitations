<!doctype html>
<html lang="en">
<head>
    <title>E-Travel Docs</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ $public_path }}css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ $public_path }}fonts/font-awesome/css/font-awesome.min.css">
    <!-- Open Sans -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ $public_path }}css/style.css">
</head>

<body class="etd--homepage etd-en">
<header class="etd__header">
    <div class="wrapper">
        <nav class="etd__header__nav navbar navbar-expand">
            <a class="etd__header__nav__logo d-none d-md-block navbar-brand" href="#"><img src="{{ $public_path }}img/logo/logo.png"
                                                                                           alt="Logo"></a>
            <a class="etd__header__nav__logo d-md-none navbar-brand" href="#"><img src="{{ $public_path }}img/logo/logo-sm.png"
                                                                                   alt="Logo"></a>

            <ul class="etd__header__nav__list navbar-nav">
                <li class="etd__header__nav__list__item nav-item">
                    <a href="#" target="_blank">faq</a>
                </li>

                <li class="etd__header__nav__list__item nav-item dropdown">
                    <button class="lang-btn nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          		<span class="wrap">
					      			<span class="lang-btn__short">en</span>
					      			<span class="lang-btn__long">english<img src="{{ $public_path }}img/flag/english.png" alt=""></span>
					      		</span>
                    </button>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="lang-btn__short dropdown-item" href="#">de</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/de">deutsch<img src="{{ $public_path }}img/flag/german.png"
                                                                                                             alt="German flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">da</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/da">danish<img src="{{ $public_path }}img/flag/danish.png"
                                                                                                            alt=""></a>

                        <a class="lang-btn__short dropdown-item" href="#">fi</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/fi">finnish<img src="{{ $public_path }}img/flag/finnish.png"
                                                                                                             alt="Finnish flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">fr</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/fr">french<img src="{{ $public_path }}img/flag/french.png"
                                                                                                            alt="French flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">it</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/it">italian<img src="{{ $public_path }}img/flag/italian.jpg"
                                                                                                             alt="Italian flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">nl</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/nl">dutch<img src="{{ $public_path }}img/flag/dutch.jpg"
                                                                                                           alt="Deutch flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">no</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/no">norwegian<img src="{{ $public_path }}img/flag/norwey.png"
                                                                                                               alt="Norwegian flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">pl</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/pl">polish<img src="{{ $public_path }}img/flag/polish.png"
                                                                                                            alt="Polosh flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">es</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/es">spanish<img src="{{ $public_path }}img/flag/spanish.png"
                                                                                                             alt="Spanish flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">sw</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/sw">swedish<img src="{{ $public_path }}img/flag/swedish.png"
                                                                                                             alt="Swedish flag"></a>
                    </div>
                </li>
            </ul>
        </nav>
    </div><!-- /wrapper -->
</header><!-- /etd__header -->
<main class="etd__body">
    <!-- $Banner section -->
    <section class="etd__body__banner">
        <div class="wrapper">
            <div class="etd__body__banner__box">
                <h1>Thanks for your translation.</h1>

                <h3>You will be notified about the payment.</h3>


            </div><!-- /etd__body__banner__box -->
        </div><!-- /wrapper -->
    </section>
    <!-- /etd__body__banner -->



</main><!-- /etd__body -->

<footer>
    <div class="wrapper">
        <div class="f-header">
            <h2 class="f-header__heading">Subscribe to our awesome Newsletter</h2>

            <div class="wrapp">
                <input type="text" class="f-header__sign-up-input" placeholder="Enter your email address"
                       aria-label="Enter your email address">

                <button type="button" class="sign-up-btn">Sign Up</button>
            </div><!-- /wrapp -->
        </div><!-- /f-header -->

        <div class="f-body">
            <div class="f-body__top-box">
                <img class="d-none d-lg-block" src="{{ $public_path }}img/logo/logo-white.png" alt="Logo">
                <img class="d-lg-none" src="{{ $public_path }}img/logo/logo-white-sm.png" alt="Logo">

                <div class="wrapp">
                    <a href="#">Russia Tourist invitation</a>

                    <a href="#">Russia Business Invitation</a>

                    <a href="#">Belarus Tourist Invitation</a>

                    <a href="#">Belarus Visa on arrival</a>
                </div><!-- /wrapp -->
            </div><!-- /f-body__top-box -->

            <div class="f-body__btm-box">
                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="{{ $public_path }}img/icon/phone.png" alt="Phone icon">
                    <span>You can call us:</span>
                    <span><a href="tel:+41315281687">+ 41 315281687</a></span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="{{ $public_path }}img/icon/email.png" alt="email icon">
                    <span>You can emall us:</span>
                    <span><a href="mailto:hello@etraveldocs.com">hello@etraveldocs.com</a></span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="{{ $public_path }}img/icon/customer-support.png" alt="Customer support icon">
                    <span>Customer support:</span>
                    <span>Monday/Friday - 09:00 - 17:00</span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__soc-net">
                    <a href=""><img src="{{ $public_path }}img/icon/facebook.png" alt="Facebook icon"></a>
                    <a href=""><img src="{{ $public_path }}img/icon/twitter.png" alt="Twitter icon"></a>
                    <a href=""><img src="{{ $public_path }}img/icon/linkedin.png" alt="LinkedIn icon"></a>
                    <a href=""><img src="{{ $public_path }}img/icon/instagram.png" alt="Instagram icon"></a>
                </div><!-- /f-body__btm-box__soc-net -->
            </div><!-- /f-body__btm-box -->
        </div><!-- /f-body -->

        <div class="f-footer">
            All rights reserved bu Etraveldocs AG
        </div><!-- /f-footer -->
    </div><!-- /wrapper -->
</footer>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ $public_path }}js/jquery-3.2.1.slim.min.js"></script>
<script src="{{ $public_path }}js/popper.min.js"></script>
<script src="{{ $public_path }}js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script src="{{ $public_path }}js/custom.js"></script>
</body>
</html>