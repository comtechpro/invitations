<!doctype html>
<html lang="en">
<head>
    <title>E-Travel Docs</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ $public_path }}css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ $public_path }}fonts/font-awesome/css/font-awesome.min.css">
    <!-- Open Sans -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ $public_path }}css/style.css">
</head>

<body class="etd--homepage etd-en">
<header class="etd__header">
    <div class="wrapper">
        <nav class="etd__header__nav navbar navbar-expand">
            <a class="etd__header__nav__logo d-none d-md-block navbar-brand" href="#"><img src="{{ $public_path }}img/logo/logo.png"
                                                                                           alt="Logo"></a>
            <a class="etd__header__nav__logo d-md-none navbar-brand" href="#"><img src="{{ $public_path }}img/logo/logo-sm.png"
                                                                                   alt="Logo"></a>

            <ul class="etd__header__nav__list navbar-nav">
                <li class="etd__header__nav__list__item nav-item">
                    <a href="#" target="_blank">faq</a>
                </li>

                <li class="etd__header__nav__list__item nav-item dropdown">
                    <button class="lang-btn nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          		<span class="wrap">
					      			<span class="lang-btn__short">sw</span>
					      			<span class="lang-btn__long">swedish<img src="{{ $public_path }}img/flag/swedish.png" alt=""></span>
					      		</span>
                    </button>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="lang-btn__short dropdown-item" href="#">en</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/en">english<img src="{{ $public_path }}img/flag/english.png"
                                                                                                             alt=""></a>
                        <a class="lang-btn__short dropdown-item" href="#">de</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/de">deutsch<img src="{{ $public_path }}img/flag/german.png"
                                                                                                              alt="German flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">da</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/da">danish<img src="{{ $public_path }}img/flag/danish.png"
                                                                                                             alt=""></a>

                        <a class="lang-btn__short dropdown-item" href="#">fi</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/fi">finnish<img src="{{ $public_path }}img/flag/finnish.png"
                                                                                                              alt="Finnish flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">fr</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/fr">french<img src="{{ $public_path }}img/flag/french.png"
                                                                                                             alt="French flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">it</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/it">italian<img src="{{ $public_path }}img/flag/italian.jpg"
                                                                                                              alt="Italian flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">nl</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/nl">dutch<img src="{{ $public_path }}img/flag/dutch.jpg"
                                                                                                            alt="Deutch flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">no</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/no">norwegian<img src="{{ $public_path }}img/flag/norwey.png"
                                                                                                                alt="Norwegian flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">pl</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/pl">polish<img src="{{ $public_path }}img/flag/polish.png"
                                                                                                             alt="Polosh flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">es</a>
                        <a class="lang-btn__long dropdown-item" href="{{ $public_path }}lang/es">spanish<img src="{{ $public_path }}img/flag/spanish.png"
                                                                                                              alt="Spanish flag"></a>

                    </div>
                </li>
            </ul>
        </nav>
    </div><!-- /wrapper -->
</header><!-- /etd__header -->
<main class="etd__body">
    <!-- $Banner section -->
    <section class="etd__body__banner">
        <div class="wrapper">
            <div class="etd__body__banner__box">
                <h1>Behöver du en Rysslands inbjudan? Du är på rätt ställe. </h1>

                <h3>Apply on our website today</h3>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="etd__body__banner__box__col col-full active">
                                <div class="etd__body__banner__box__col__top-triangle"></div>

                                <h2>Russia Tourist Invitation</h2>

                                <hr>

                                <div class="etd__body__banner__box__col__apply">
                                    <div class="wrapp">
                                        <span class="etd__body__banner__box__col__apply__cur"> {{ $currency }}</span>

                                        <span class="etd__body__banner__box__col__apply__fee">{{ $price }}</span>

                                        <span class="etd__body__banner__box__col__apply__fee small--font"> {{ $cents }}</span>
                                    </div>

                                    <button class="apply-btn">Apply now</button>
                                </div><!-- /etd__body__banner__box__col__apply -->
                            </div><!-- /etd__body__banner__box__col -->
                        </div><!-- /col-md-6 col-lg-6 -->

                        <div class="col-md-6 col-lg-6">
                            <div class="etd__body__banner__box__col">
                                <h2>Russia Business Invitation</h2>

                                <hr>

                                <div class="wrapp">
                                    <span>Coming soon</span>
                                </div>
                            </div><!-- /etd__body__banner__box__col -->
                        </div><!-- /col-md-6 col-lg-6 -->

                        <div class="col-md-6 col-lg-6">
                            <div class="etd__body__banner__box__col">
                                <h2>Belarus Tourist Invitation</h2>

                                <hr>

                                <div class="wrapp">
                                    <span>Coming soon</span>
                                </div>
                            </div><!-- /etd__body__banner__box__col -->
                        </div><!-- /col-md-6 col-lg-6 -->

                        <div class="col-md-6 col-lg-6">
                            <div class="etd__body__banner__box__col">
                                <h2>Vietnam Visa on arrival</h2>

                                <hr>

                                <div class="wrapp">
                                    <span>Coming soon</span>
                                </div>
                            </div><!-- /etd__body__banner__box__col -->
                        </div><!-- /col-md-6 col-lg-6 -->
                    </div>
                </div>
            </div><!-- /etd__body__banner__box -->
        </div><!-- /wrapper -->
    </section>
    <!-- /etd__body__banner -->

    <!-- $Form section -->
    <section class="etd__form">
        <div class="wrapper">
            <div class="container-fluid">
                <form id="infoForm" action="processing" method="post">
                    <div class="first-section">
                        <div class="row">
                            <div class="etd__form__step-1 col-md-12 col-lg-6">
                                <h4 class="etd__form__step__heading">Step 1 / Your personal details</h4>

                                <div class="row">
                                    <div class="col-md-6 col-lg-12 col-xl-6">
                                        <div class="steps__f-grp form-group">
                                            <label for="email">Your email address</label>
                                            <input id="email" class="form-control" type="email" name="email">
                                        </div>

                                        <div class="steps__f-grp form-group">
                                            <label for="citizenship">Citizenships</label>
                                            <input id="citizenship" class="form-control" type="text" name="citizenship">
                                        </div>

                                        <div class="steps__f-grp form-group">
                                            <label for="first_name">First Name</label>
                                            <input id="first_name" class="form-control" type="text" name="first_name">
                                        </div>

                                        <div class="steps__f-grp form-group">
                                            <label for="last_name">Last Name</label>
                                            <input id="last_name" class="form-control" type="text" name="last_name">
                                        </div>

                                        <div class="steps__f-grp form-group">
                                            <label for="date_of_birth">Date of birth (dd/mm/yyyy)</label>
                                            <input id="date_of_birth" class="form-control" name="date_of_birth">
                                            <!--<span class="input-group-btn">-->
                                            <!--<button class="steps__f-grp__dtp-btn btn" type="button"><img src="img/icon/dt-picker.png" alt="Date-time picker icon"></button>-->
                                            <!--</span>-->

                                        </div>

                                    </div>

                                    <div class="col-md-6 col-lg-12 col-xl-6">

                                        <div class="steps__f-grp form-group">
                                            <label for="confirmemail">Confirm your email address</label>
                                            <input id="confirmemail" class="form-control" type="email" name="confirmemail">
                                        </div>

                                        <div class="steps__f-grp form-group">
                                            <p>Number of entries</p>
                                            <div class="steps__f-grp__btn--wrapp">

                                                <label class="steps__f-grp__btn ">
                                                    <label style="vertical-align: top;">Single</label>
                                                    <input class="radio-input" type="radio" name="entries">
                                                </label>


                                                <label class="steps__f-grp__btn">
                                                    <label style="vertical-align: top;">Double</label>
                                                    <input class="radio-input" type="radio" name="entries">
                                                </label>
                                            </div>
                                        </div>

                                        <div class="steps__f-grp form-group">
                                            <label for="mid_name">Middle Name</label>
                                            <input id="mid_name" class="form-control" type="text" name="middle_name">
                                        </div>

                                        <div class="steps__f-grp form-group">
                                            <p>Gender</p>
                                            <div class="steps__f-grp__btn--wrapp">
                                                <label class="steps__f-grp__btn ">
                                                    <label style="vertical-align: top;">Male</label>
                                                    <input class="radio-input" type="radio" name="gender">
                                                </label>
                                                <label class="steps__f-grp__btn">
                                                    <label style="vertical-align: top;">Female</label>
                                                    <input class="radio-input" type="radio" name="gender">
                                                </label>
                                            </div>
                                        </div>

                                        <div class="steps__f-grp form-group">
                                            <label for="pass_number">Passport number</label>
                                            <input id="pass_number" class="form-control" type="number" name="pass_number">
                                        </div>

                                    </div>
                                </div>

                                <div class="etd__form__step-1__add-more">
                                    <span>Add more persons traveling with you</span>
                                    <button id="addPerson">+</button>
                                </div>
                            </div>

                            <div class="etd__form__step-2 col-md-12 col-lg-6">
                                <div class="row">
                                    <div class="etd__form__step-2__form col-md-6 col-lg-12 col-xl-6">
                                        <h4 class="etd__form__step__heading">Step 2 / Trip info</h4>

                                        <div class="steps__f-grp form-group">
                                            <label for="dateEntry">Date of entry (dd/mm/yyyy)</label>
                                            <input id="dateEntry" type="text" class="form-control" name="dateEntry">
                                        </div>

                                        <div class="steps__f-grp form-group">
                                            <label for="dateExit">Date of exit (dd/mm/yyyy)</label>
                                            <input id="dateExit" type="text" class="form-control" name="dateExit">
                                        </div>

                                        <!-- button class="steps__f-grp__btn-more" should generate another div class="steps__f-grp__accomo" -->
                                        <div class="steps__f-grp form-group">
                                            <label><i class="fa fa-info-circle"></i>Name of accommodation</label>
                                            <div class="steps__f-grp__accomo input-group">
                                                <input type="text" class="input--margin-r form-control"
                                                       aria-label="Name of accommodation">

                                                <span class="input-group-btn">
														<button id='add_field_button' class="steps__f-grp__btn-more btn btn-secondary " type="button">+</button>
													</span>
                                            </div>
                                        </div>
                                        <div class="steps__f-grp form-group">
                                            <div id="input_fields_wrap"></div>
                                        </div>

                                        <div class="steps__f-grp form-group">
                                            <label>Neighboring countries to visit (first entry)</label>

                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Select"
                                                       aria-label="Neighboring countries to visit, first entry">

                                                <div class="steps__f-grp__btn-grp input-group-btn">
                                                    <button type="button"
                                                            class="steps__f-grp__dropdown-btn btn btn-secondary dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                    </button>

                                                    <div class="steps__f-grp__dropdown-menu dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="#">Action</a>

                                                        <a class="dropdown-item" href="#">Another action</a>

                                                        <a class="dropdown-item" href="#">Something else here</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="steps__f-grp form-group">
                                            <label for="pers_city">City</label>
                                            <input id="pers_city" class="form-control" type="text" name="pers_city">
                                        </div>
                                    </div>

                                    <div class="etd__form__step-2__payment col-md-6 col-lg-12 col-xl-6">
                                        <h4>Total price for your Russia Tourist Invitation</h4>

                                        <div class="divider"></div>

                                        <div class="payment__box">
                                            <span class="payment__box__cur">{{ $currency }}</span>
                                            <span class="payment__box__fee">{{ $price }}</span>
                                            <span class="payment__box__fee--sm">{{ $cents }}</span>
                                        </div>

                                        <label class="payment__checkbox custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Check this custom checkbox</span>
                                        </label>

                                        <button class="apply-btn" type="submit">payment</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="otherPeople"></div>
                </form>
            </div>
        </div>
    </section>
    <!-- $FAQ section -->

    <section class="etd__body__faq">
        <div class="wrapper">
            <div class="etd__body__faq__container">
                <h2>FAQ's</h2>

                <p>To help, we’ve provided answers to the most frequently-asked questions that we’re posed. <span>However if you are still unsure or would like to know more, please do get in contact with us.</span>
                </p>

                <hr>

                <div class="etd__body__faq__container__input input-group">
                    <input type="text" class="form-control" placeholder="Search for question"
                           aria-label="Search for...">

                    <span class="input-group-btn">
					        	<button class="btn btn-secondary" type="button"><span class="fa fa-search"
                                                                                      aria-hidden="true"></span></button>
					      	</span>
                </div><!-- /etd__body__faq__container__input input-group -->

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg etd__body__faq__container__q-box--wrapp">
                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can I pay for visa invitation online?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Yes, you can pay for visa invitation with your credit card Visa or Mastercard
                                        during the complition application form. You should make sure in advance that
                                        your credit card account has sufficient funds to pay that amount.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can I pay with someone else's credit card?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Yes, you can.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Is it possible to make a refund?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Unfortunately no. payment for visa service is not refundable.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>How long will it take for me to receive my invitations after purchase?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>It’s automatic system. That’s why after the successful payment your invitation
                                        will be issued and sent to your e-mail. You will need just print it out and
                                        apply for a visa.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can you modify my invitation letter if the dates, name or some other information
                                        is incorrect?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>This information you should specify in the Consulate of Russian Federation. As we
                                        do not deal with any Consulates we are not able to inform you regarding this
                                        question.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->
                        </div><!-- /col-lg etd__body__faq__container__q-box--wrapp -->

                        <div class="col-lg etd__body__faq__container__q-box--wrapp">
                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can I pay for visa invitation online?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Yes, you can pay for visa invitation with your credit card Visa or Mastercard
                                        during the complition application form. You should make sure in advance that
                                        your credit card account has sufficient funds to pay that amount.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can I pay with someone else's credit card?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Yes, you can.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Is it possible to make a refund?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Unfortunately no. payment for visa service is not refundable.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>How long will it take for me to receive my invitations after purchase?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>It’s automatic system. That’s why after the successful payment your invitation
                                        will be issued and sent to your e-mail. You will need just print it out and
                                        apply for a visa.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can you modify my invitation letter if the dates, name or some other information
                                        is incorrect?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>This information you should specify in the Consulate of Russian Federation. As we
                                        do not deal with any Consulates we are not able to inform you regarding this
                                        question.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->
                        </div><!-- /col-lg etd__body__faq__container__q-box--wrapp -->
                    </div><!-- /row -->
                </div><!-- /container-fluid -->

                <button class="more-faq-btn">More FAQ</button>
            </div><!-- /etd__body__faq__container -->
        </div><!-- /wrapper -->
    </section><!-- /etd__body__faq -->
</main><!-- /etd__body -->

<footer>
    <div class="wrapper">
        <div class="f-header">
            <h2 class="f-header__heading">Subscribe to our awesome Newsletter</h2>

            <div class="wrapp">
                <input type="text" class="f-header__sign-up-input" placeholder="Enter your email address"
                       aria-label="Enter your email address">

                <button type="button" class="sign-up-btn">Sign Up</button>
            </div><!-- /wrapp -->
        </div><!-- /f-header -->

        <div class="f-body">
            <div class="f-body__top-box">
                <img class="d-none d-lg-block" src="{{ $public_path }}img/logo/logo-white.png" alt="Logo">
                <img class="d-lg-none" src="{{ $public_path }}img/logo/logo-white-sm.png" alt="Logo">

                <div class="wrapp">
                    <a href="#">Russia Tourist invitation</a>

                    <a href="#">Russia Business Invitation</a>

                    <a href="#">Belarus Tourist Invitation</a>

                    <a href="#">Belarus Visa on arrival</a>
                </div><!-- /wrapp -->
            </div><!-- /f-body__top-box -->

            <div class="f-body__btm-box">
                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="{{ $public_path }}img/icon/phone.png" alt="Phone icon">
                    <span>You can call us:</span>
                    <span><a href="tel:+41315281687">+ 41 315281687</a></span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="{{ $public_path }}img/icon/email.png" alt="email icon">
                    <span>You can emall us:</span>
                    <span><a href="mailto:hello@etraveldocs.com">hello@etraveldocs.com</a></span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="{{ $public_path }}img/icon/customer-support.png" alt="Customer support icon">
                    <span>Customer support:</span>
                    <span>Monday/Friday - 09:00 - 17:00</span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__soc-net">
                    <a href=""><img src="{{ $public_path }}img/icon/facebook.png" alt="Facebook icon"></a>
                    <a href=""><img src="{{ $public_path }}img/icon/twitter.png" alt="Twitter icon"></a>
                    <a href=""><img src="{{ $public_path }}img/icon/linkedin.png" alt="LinkedIn icon"></a>
                    <a href=""><img src="{{ $public_path }}img/icon/instagram.png" alt="Instagram icon"></a>
                </div><!-- /f-body__btm-box__soc-net -->
            </div><!-- /f-body__btm-box -->
        </div><!-- /f-body -->

        <div class="f-footer">
            All rights reserved bu Etraveldocs AG
        </div><!-- /f-footer -->
    </div><!-- /wrapper -->
</footer>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ $public_path }}js/jquery-3.2.1.slim.min.js"></script>
<script src="{{ $public_path }}js/popper.min.js"></script>
<script src="{{ $public_path }}js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script src="{{ $public_path }}js/custom.js"></script>
</body>
</html>