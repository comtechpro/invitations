<form action="/{{ $action }}" method="POST">
       <script
      src="https://checkout.stripe.com/checkout.js" class="stripe-button"
            data-key="{{ $stripeToken }}"
            data-amount="{{ $amount}}"
            data-currency="{{ $currency }}"
            data-name="stripe.com"
            data-description="{{ $invitations }}"
            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
            data-locale="auto"
            data-zip-code="true">
        </script>
    </form>
