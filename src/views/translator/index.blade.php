<!doctype html>
<html lang="en">
<head>
    <title>E-Travel Docs</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ $public_path }}css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ $public_path }}fonts/font-awesome/css/font-awesome.min.css">
    <!-- Open Sans -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ $public_path }}css/style.css">
</head>

<body class="etd--homepage etd-en">
<header class="etd__header">
    <div class="wrapper">

    </div><!-- /wrapper -->
</header><!-- /etd__header -->
<main class="etd__body">
    <!-- $Banner section -->
    <section class="etd__body__banner">
        <div class="wrapper">
            <div class="etd__body__banner__box">
                <h1>Brauchen Sie eine Russland-Einladung? Sie sind an der richtigen Stelle. </h1>

                <h3>Apply on our website today</h3>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="etd__body__banner__box__col col-full active">
                                <div class="etd__body__banner__box__col__top-triangle"></div>

                                <h2>Russia Tourist Invitation</h2>

                                <hr>

                                <div class="etd__body__banner__box__col__apply">
                                    <div class="wrapp">
                                        <span class="etd__body__banner__box__col__apply__cur"> {{ $currency }}</span>

                                        <span class="etd__body__banner__box__col__apply__fee">{{ $price }}</span>

                                        <span class="etd__body__banner__box__col__apply__fee small--font"> {{ $cents }}</span>
                                    </div>

                                    <button class="apply-btn">Apply now</button>
                                </div><!-- /etd__body__banner__box__col__apply -->
                            </div><!-- /etd__body__banner__box__col -->
                        </div><!-- /col-md-6 col-lg-6 -->

                        <div class="col-md-6 col-lg-6">
                            <div class="etd__body__banner__box__col">
                                <h2>Russia Business Invitation</h2>

                                <hr>

                                <div class="wrapp">
                                    <span>Coming soon</span>
                                </div>
                            </div><!-- /etd__body__banner__box__col -->
                        </div><!-- /col-md-6 col-lg-6 -->

                        <div class="col-md-6 col-lg-6">
                            <div class="etd__body__banner__box__col">
                                <h2>Belarus Tourist Invitation</h2>

                                <hr>

                                <div class="wrapp">
                                    <span>Coming soon</span>
                                </div>
                            </div><!-- /etd__body__banner__box__col -->
                        </div><!-- /col-md-6 col-lg-6 -->

                        <div class="col-md-6 col-lg-6">
                            <div class="etd__body__banner__box__col">
                                <h2>Vietnam Visa on arrival</h2>

                                <hr>

                                <div class="wrapp">
                                    <span>Coming soon</span>
                                </div>
                            </div><!-- /etd__body__banner__box__col -->
                        </div><!-- /col-md-6 col-lg-6 -->
                    </div>
                </div>
            </div><!-- /etd__body__banner__box -->
        </div><!-- /wrapper -->
    </section>
    <!-- /etd__body__banner -->

    <!-- $Form section -->
    <section class="etd__form">
        <div class="wrapper">
            <div class="container-fluid">
                <form id="infoForm" action="/translate/store" method="post">
                    <div class="first-section">
                        <div class="row">

                            <div class="etd__form__step-2 col-md-12 col-lg-6">
                                <div class="row">
                                    <div class="etd__form__step-2__form col-md-6 col-lg-12 col-xl-6">
                                        <h4 class="etd__form__step__heading">Translate this name</h4>

                                        <div class="steps__f-grp form-group">
                                            <label for="name">Name: {{ $origin_name }}</label>
                                            <input id="name" type="text" class="form-control" name="name">
                                        </div>

                                        <div class="steps__f-grp form-group">
                                            <label for="name">Translator: {{ $id }}</label>
                                            <input id="translator" type="text" class="form-control" name="translator">
                                        </div>







                                    </div>

                                    <div class="etd__form__step-2__payment col-md-6 col-lg-12 col-xl-6">





                                        <button class="apply-btn" type="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="otherPeople"></div>
                </form>
            </div>
        </div>
    </section>
    <!-- $FAQ section -->

    <section class="etd__body__faq">
        <div class="wrapper">
            <div class="etd__body__faq__container">
                <h2>FAQ's</h2>

                <p>To help, we’ve provided answers to the most frequently-asked questions that we’re posed. <span>However if you are still unsure or would like to know more, please do get in contact with us.</span>
                </p>

                <hr>

                <div class="etd__body__faq__container__input input-group">
                    <input type="text" class="form-control" placeholder="Search for question"
                           aria-label="Search for...">

                    <span class="input-group-btn">
					        	<button class="btn btn-secondary" type="button"><span class="fa fa-search"
                                                                                      aria-hidden="true"></span></button>
					      	</span>
                </div><!-- /etd__body__faq__container__input input-group -->

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg etd__body__faq__container__q-box--wrapp">
                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can I pay for visa invitation online?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Yes, you can pay for visa invitation with your credit card Visa or Mastercard
                                        during the complition application form. You should make sure in advance that
                                        your credit card account has sufficient funds to pay that amount.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can I pay with someone else's credit card?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Yes, you can.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Is it possible to make a refund?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Unfortunately no. payment for visa service is not refundable.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>How long will it take for me to receive my invitations after purchase?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>It’s automatic system. That’s why after the successful payment your invitation
                                        will be issued and sent to your e-mail. You will need just print it out and
                                        apply for a visa.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can you modify my invitation letter if the dates, name or some other information
                                        is incorrect?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>This information you should specify in the Consulate of Russian Federation. As we
                                        do not deal with any Consulates we are not able to inform you regarding this
                                        question.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->
                        </div><!-- /col-lg etd__body__faq__container__q-box--wrapp -->

                        <div class="col-lg etd__body__faq__container__q-box--wrapp">
                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can I pay for visa invitation online?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Yes, you can pay for visa invitation with your credit card Visa or Mastercard
                                        during the complition application form. You should make sure in advance that
                                        your credit card account has sufficient funds to pay that amount.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can I pay with someone else's credit card?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Yes, you can.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Is it possible to make a refund?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Unfortunately no. payment for visa service is not refundable.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>How long will it take for me to receive my invitations after purchase?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>It’s automatic system. That’s why after the successful payment your invitation
                                        will be issued and sent to your e-mail. You will need just print it out and
                                        apply for a visa.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can you modify my invitation letter if the dates, name or some other information
                                        is incorrect?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>This information you should specify in the Consulate of Russian Federation. As we
                                        do not deal with any Consulates we are not able to inform you regarding this
                                        question.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->
                        </div><!-- /col-lg etd__body__faq__container__q-box--wrapp -->
                    </div><!-- /row -->
                </div><!-- /container-fluid -->

                <button class="more-faq-btn">More FAQ</button>
            </div><!-- /etd__body__faq__container -->
        </div><!-- /wrapper -->
    </section><!-- /etd__body__faq -->
</main><!-- /etd__body -->

<footer>
    <div class="wrapper">
        <div class="f-header">
            <h2 class="f-header__heading">Subscribe to our awesome Newsletter</h2>

            <div class="wrapp">
                <input type="text" class="f-header__sign-up-input" placeholder="Enter your email address"
                       aria-label="Enter your email address">

                <button type="button" class="sign-up-btn">Sign Up</button>
            </div><!-- /wrapp -->
        </div><!-- /f-header -->

        <div class="f-body">
            <div class="f-body__top-box">
                <img class="d-none d-lg-block" src="{{ $public_path }}img/logo/logo-white.png" alt="Logo">
                <img class="d-lg-none" src="{{ $public_path }}img/logo/logo-white-sm.png" alt="Logo">

                <div class="wrapp">
                    <a href="#">Russia Tourist invitation</a>

                    <a href="#">Russia Business Invitation</a>

                    <a href="#">Belarus Tourist Invitation</a>

                    <a href="#">Belarus Visa on arrival</a>
                </div><!-- /wrapp -->
            </div><!-- /f-body__top-box -->

            <div class="f-body__btm-box">
                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="{{ $public_path }}img/icon/phone.png" alt="Phone icon">
                    <span>You can call us:</span>
                    <span><a href="tel:+41315281687">+ 41 315281687</a></span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="{{ $public_path }}img/icon/email.png" alt="email icon">
                    <span>You can emall us:</span>
                    <span><a href="mailto:hello@etraveldocs.com">hello@etraveldocs.com</a></span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="{{ $public_path }}img/icon/customer-support.png" alt="Customer support icon">
                    <span>Customer support:</span>
                    <span>Monday/Friday - 09:00 - 17:00</span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__soc-net">
                    <a href=""><img src="{{ $public_path }}img/icon/facebook.png" alt="Facebook icon"></a>
                    <a href=""><img src="{{ $public_path }}img/icon/twitter.png" alt="Twitter icon"></a>
                    <a href=""><img src="{{ $public_path }}img/icon/linkedin.png" alt="LinkedIn icon"></a>
                    <a href=""><img src="{{ $public_path }}img/icon/instagram.png" alt="Instagram icon"></a>
                </div><!-- /f-body__btm-box__soc-net -->
            </div><!-- /f-body__btm-box -->
        </div><!-- /f-body -->

        <div class="f-footer">
            All rights reserved bu Etraveldocs AG
        </div><!-- /f-footer -->
    </div><!-- /wrapper -->
</footer>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ $public_path }}js/jquery-3.2.1.slim.min.js"></script>
<script src="{{ $public_path }}js/popper.min.js"></script>
<script src="{{ $public_path }}js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script src="{{ $public_path }}js/custom.js"></script>
</body>
</html>