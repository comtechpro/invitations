<!doctype html>
<html lang="en">
<head>
    <title>translate part</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ $public_path }}css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ $public_path }}fonts/font-awesome/css/font-awesome.min.css">
    <!-- Open Sans -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ $public_path }}css/style.css">
</head>

<body class="etd--homepage etd-en">
<header class="etd__header">
    <div class="wrapper">

    </div><!-- /wrapper -->
</header><!-- /etd__header -->
<main class="etd__body">
    <!-- $Form section -->
    <section class="etd__form">
        <div class="wrapper">
            <div class="container-fluid">
                <form id="infoForm" action="{{ $public_path }}lang/{{ $lang }}/translate/store" method="post">
                    <div class="first-section">
                        <div class="row">
                            <div class="etd__form__step-1 col-md-12 col-lg-6">
                                <h4 class="etd__form__step__heading">Please, enter the valid translation and submit... Thx </h4>
                                <div class="row">
                                    <div class="col-md-6 col-lg-12 col-xl-6">
                                        <div class="steps__f-grp form-group">
                                            <input hidden name="originName" value={{ $originName }}>
                                            <input hidden name="token" value={{ $token }}>
                                            <input hidden name="translatorId" value={{ $translatorId }}>
                                            <label for="translatedName">Enter translatation for name : {{ $originName }} </label>
                                            <input id="translatedName" class="form-control" type="text" name="translatedName">
                                        </div>
                                    </div>
                                </div>
                                <button class="apply-btn" type="submit">Translate</button>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- $FAQ section -->


</main><!-- /etd__body -->

<footer>
    <div class="wrapper">
        <div class="f-header">
            <h2 class="f-header__heading">Subscribe to our awesome Newsletter</h2>

            <div class="wrapp">
                <input type="text" class="f-header__sign-up-input" placeholder="Enter your email address"
                       aria-label="Enter your email address">

                <button type="button" class="sign-up-btn">Sign Up</button>
            </div><!-- /wrapp -->
        </div><!-- /f-header -->

        <div class="f-body">
            <div class="f-body__top-box">
                <img class="d-none d-lg-block" src="{{ $public_path }}img/logo/logo-white.png" alt="Logo">
                <img class="d-lg-none" src="{{ $public_path }}img/logo/logo-white-sm.png" alt="Logo">

                <div class="wrapp">
                    <a href="#">Russia Tourist invitation</a>

                    <a href="#">Russia Business Invitation</a>

                    <a href="#">Belarus Tourist Invitation</a>

                    <a href="#">Belarus Visa on arrival</a>
                </div><!-- /wrapp -->
            </div><!-- /f-body__top-box -->

            <div class="f-body__btm-box">
                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="{{ $public_path }}img/icon/phone.png" alt="Phone icon">
                    <span>You can call us:</span>
                    <span><a href="tel:+41315281687">+ 41 315281687</a></span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="{{ $public_path }}img/icon/email.png" alt="email icon">
                    <span>You can emall us:</span>
                    <span><a href="mailto:hello@etraveldocs.com">hello@etraveldocs.com</a></span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="{{ $public_path }}img/icon/customer-support.png" alt="Customer support icon">
                    <span>Customer support:</span>
                    <span>Monday/Friday - 09:00 - 17:00</span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__soc-net">
                    <a href=""><img src="{{ $public_path }}img/icon/facebook.png" alt="Facebook icon"></a>
                    <a href=""><img src="{{ $public_path }}img/icon/twitter.png" alt="Twitter icon"></a>
                    <a href=""><img src="{{ $public_path }}img/icon/linkedin.png" alt="LinkedIn icon"></a>
                    <a href=""><img src="{{ $public_path }}img/icon/instagram.png" alt="Instagram icon"></a>
                </div><!-- /f-body__btm-box__soc-net -->
            </div><!-- /f-body__btm-box -->
        </div><!-- /f-body -->

        <div class="f-footer">
            All rights reserved bu Etraveldocs AG
        </div><!-- /f-footer -->
    </div><!-- /wrapper -->
</footer>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ $public_path }}js/jquery-3.2.1.slim.min.js"></script>
<script src="{{ $public_path }}js/popper.min.js"></script>
<script src="{{ $public_path }}js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script src="{{ $public_path }}js/custom.js"></script>
</body>
</html>