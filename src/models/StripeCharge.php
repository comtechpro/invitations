<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class StripeCharge
 * @package App\Models
 */
class StripeCharge extends Eloquent
{
    protected $table = 'stripe_charges';

    public $id;
    public $object;
    public $amount;
    public $balance_transaction;
    public $currency;
    public $description;
    public $paid;
    public $status;

    protected $fillable = [
        'id',
        'object',
        'amount',
        'balance_transaction',
        'currency',
        'description',
        'paid',
        'status',
    ];
}
