<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class StripeToken
 * @package App\Invitation\Models
 */
class StripeToken extends Eloquent
{
    protected $table = 'stripe_tokens';

    public $stripeToken;
    public $stripeTokenType;
    public $stripeEmail;

    protected $fillable = [
        'stripeToken',
        'stripeTokenType',
        'stripeEmail',
    ];
}
