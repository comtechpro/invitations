<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Translator
 * @package Viselio\Api
 */
class Translator extends Eloquent
{
    protected $fillable = [
        '$email',
        '$firstName',
        '$lastName',
    ];

    /** @var  string */
    private $email;

    /** @var  string */
    private $firstName;

    /** @var  string */
    private $lastName;


    /** string */
    public function getEmail() : string
    {
        return $this->email;
    }

    /** string */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /** string */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /** string */
    public function getLanguages(): array
    {
        return $this->languages;
    }
}
