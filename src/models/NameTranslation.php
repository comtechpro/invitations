<?php

declare(strict_types = 1);

namespace App\Models;

use App\Services\NameTranslationHelper;
use SplObserver;
use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class NameTranslation
 * @package App\Models
 */
 class NameTranslation extends Eloquent
 {
     public $table = 'name_translations';

     protected $fillable = [
         'origin_language_id',
         'origin_name',
         'translated_language_id',
         'translated_name',
         'translator_id',
         'status',
         'token'
    ];

     public const TRANSLATED = 1;
     public const PENDING = 0;

//    list of languages
     public const GERMAN_LANGUAGE = 1;
     public const RUSSIAN_LANGUAGE = 2;

     /** @var int */
     public $origin_language_id;

     /** @var  string */
     public $origin_name;

     /** @var int */
     public $translated_language_id;

     /** @var  string */
     public $translated_name;

     /** @var int */
     protected $translator_id;

     /** @var int */
     protected $status;

     /** @var  string */
     protected $token;

     /** @var \SplObjectStorage  */
     protected $observers;

     public $newBaseQueryBuilder;

     public function __construct(array $attributes = [])
     {
         parent::__construct($attributes);
         $this->newBaseQueryBuilder = $this->newBaseQueryBuilder()->from('name_translations');
     }

     /**
      * @param SplObserver $observer
      * @return NameTranslation
      */
     public function attach(SplObserver $observer) : NameTranslation
     {
         if (!$this->observers) {
             $this->observers = new \SplObjectStorage();
         }

         $this->observers->attach($observer);

         return $this;
     }

     /**
      * @param string $token
      * @return NameTranslation
      */
     public function setToken($token = null) : NameTranslation
     {
         if (!$token) {
             $token = NameTranslationHelper::createNameTranslationToken();
         }

         $this->token = $token;

         return $this;
     }

     public function getStatus()
     {
         return $this->status;
     }

     public function getToken()
     {
         if (!$this->token) {
             $this->setToken();
         }

         return $this->token;
     }

     /**
      * @param $status
      * @return NameTranslation
      */
     public function setStatus($status) : NameTranslation
     {
         $this->status = $status;

         return $this;
     }
 }
