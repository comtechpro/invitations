<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class LanguageTranslator
 * @package Viselio\Api\NameTranslation
 */
class LanguageTranslator extends Eloquent
{
    public $language_id;
    public $translator_id;

    protected $fillable = [
        'language_id',
        'translator_id'
    ];
}
