<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Invitation
 * @package Viselio
 */
class Invitation extends Model
{
    protected $fillable = [
        'email',
        'firstName',
        'lastName',
        'origin_language_id',
        'translated_language_id',
        'dateOfBirth',
        'citizenship',
        'paymentStatus',
        'translationStatus',
        'sendStatus',
        'visaType',
        'originLanguageId',
        'translatedLanguageId'

    ];



    public const PAYMENT_STATUS_UNPAID = 0;
    public const PAYMENT_STATUS_PAID = 1;

    public const TRANSLATION_STATUS_PENDING = 0;
    public const TRANSLATION_STATUS_TRANSLATED = 1;

    public const SEND_STATUS_UNSENT = 0;
    public const SEND_STATUS_SENT = 1;

    public const VISA_TYPE_TOURIST = 1;
    public const VISA_TYPE_BUSSINESS = 2;

    /** @var string */
    public $email;

    /** @var string */
    public $firstName;

    /** @var string */
    public $translatedFirstName;

    /** @var string */
    public $lastName;

    /** @var string */
    public $translatedLastName;

    /** @var string */
    public $dateOfBirth;

    /** @var string */
    public $citizenship;

    /** @var int  */
    public $paymentStatus;

    /** @var int  */
    public $translationStatus;

    /** @var int  */
    public $sendStatus;

    /** @var int  */
    public $visaType;

    /** @var int  */
    public $origin_language_id;

    /** @var int  */
    public $translatedLanguageId;
}
