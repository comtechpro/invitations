<?php

namespace App\Services;

class EmailService
{
    /**
     * @param $email
     * @return bool
     */
    public static function checkIfValidEmail($email): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

    public static function sendEmail($to, $title, $content)
    {
        $headers = ['Content-type' => 'text/html;charset=UTF-8', 'MIME-Version' => '1.0', 'from' => 'no-reply@etraveldocs.de'];

        try {
            mail($to, $title, $content, $headers);
        } catch (\Exception $exception) {
            print_r($exception->getMessage());
            die();
        }
    }
}
