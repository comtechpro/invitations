<?php

namespace App\Services;

use Dompdf\Dompdf;

class PdfGenerator extends Fpdf
{
    public function __construct(string $orientation = 'P', string $unit = 'mm', string $size = 'A4')
    {
        parent::__construct($orientation, $unit, $size);
    }

    public function generateInvitation($invitation)
    {
//        $html = $this->buildPdfContent($invitation);
        $html = file_get_contents('http://etraveldocs.de/invitations/preview');
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
        $dompdf->render();

// Output the generated PDF to Browser
        $dompdf->stream();
//        echo $dompdf->output();
//        echo file_put_contents('/1.pdf', $dompdf->output());
    }

    public function buildPdfContent($invitation)
    {
        $html =  "<html><style>body { font-family: DejaVu Sans }</style>";
        $html .= "=<body><div style=\"margin-bottom:20px; padding: 0; border:2px solid black;\">
    <div style=\"display:inline-block; width:50%; color:#000000; border-right: 2px solid black\">
        <form action=\"\">
            <div style=\"width: 100%; padding-bottom: 15px; padding-top: 10px; padding-left: 20px\">
                <span style=\"padding-right: 25px\">ПОДТВЕРЖДЕНИЕ No</span>
                <input type=\"text\" value=\"1000\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <p style=\"padding-left: 20px; font-weight: bold\">о приеме иностранного туриста</p>
            <div style=\"width: 100%; padding-bottom: 5px;  padding-left: 20px\">
                <span style=\"padding-right: 25px\">КРАТНОСТЬ ВИЗЫ</span>
                <input type=\"text\" value=\"однократная\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">ГРАЖДАНСТВО</span>
                <input type=\"text\" value=\"Швейцария\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">ВЪЕЗД с</span>
                <input type=\"text\" value=\"17.02.2017 ВЫЕЗД по 20.02.2017\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">ФАМИЛИЯ</span>
                <input type=\"text\" value=\"Боккетти (Bocchetti)\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">ИМЯ, ОТЧЕСТВО</span>
                <input type=\"text\" value=\"Марко Андреас (Marco Andreas)\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">ДАТА РОЖДЕНИЯ</span>
                <input type=\"text\" value=\"16.03.1972 пол муж\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">НОМЕР ПАСПОРТА</span>
                <input type=\"text\" value=\"X2338202\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">ЦЕЛЬ ПОЕЗДКИ</span>
                <input type=\"text\" value=\"ТУРИЗМ\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">МАРШРУТ</span>
                <input type=\"text\" value=\"Москва\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 20px; padding-top: 20px; padding-left: 20px\">
                <span style=\"padding-right: 25px\">РАЗМЕЩЕНИЕ</span>
                <input type=\"text\" value=\"Г-ца 'Столешников Бутик-отель'\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
        </form>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <h4>Принимающая организация, ее адрес:</h4>
            <p>ООО \"Эдельвейс Тур\"</p>
            <p>191186, г. Санкт-Петербург, Итальянская ул. д.17, литер А, пом.6Н Тел: +7 (812) 382-07-08</p>
            <p>191186, St. Petersburg, Italyanskaya street 17 building A, office 6H Tel.: +7 (812) 382-07-08</p>
            <h4>No референса в МИД MBT 001772</h4>
            <h4 style=\"font-weight: normal\">ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ Индивидуальный тур</h4>
        </div>
        <div style=\"width: 100%\">
            <img src=\"./images/logo.png\" alt=\"logo\" style=\"width: 30%\">

        </div>
        <p style=\"margin-left: 25%\">Генеральный директор</p>
        <div style=\"width: 100%; position:relative\">
            <p><span style=\"float: left; margin-right: 20%\">М.П.</span>
                <span style=\"float: left; margin-right: 20%\">Воеводина Ю.А.</span>
                <span style=\"float: right; padding-right: 5px\">Дата: 19 January 2017</span>
            </p>

            <img src=\"./images/sing.png\" alt=\"sing\" style=\"width: 15%;position: absolute;bottom: 3%;left: 53%;\">

        </div>
    </div>
    <div style=\"display:inline-block; width:49%; color:#000000\">
        <form action=\"\">
            <div style=\"width: 100%; padding-bottom: 15px; padding-top: 10px; padding-left: 20px\">
                <span style=\"padding-right: 25px\">ВАУЧЕР No</span>
                <input type=\"text\" value=\"1000\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <p style=\"padding-left: 20px; font-weight: bold\">На оказание туристических услуг</p>
            <div style=\"width: 100%; padding-bottom: 5px;  padding-left: 20px\">
                <span style=\"padding-right: 25px\">КРАТНОСТЬ ВИЗЫ <span
                        style=\"font-size:11px\">Type of visa</span> </span>
                <input type=\"text\" value=\"однократная\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">ГРАЖДАНСТВО <span style=\"font-size:11px\">Citizenship</span></span>
                <input type=\"text\" value=\"Швейцария\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">ВЪЕЗД с</span>
                <input type=\"text\" value=\"17.02.2017 ВЫЕЗД по 20.02.2017\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">пол <span style=\"font-size:11px\">Sex муж</span></span>
                <input type=\"text\" value=\"Боккетти (Bocchetti)\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">ФАМИЛИЯ <span style=\"font-size:11px\">Surname</span></span>
                <input type=\"text\" value=\"Боккетти (Bocchetti)\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">ИМЯ, ОТЧЕСТВО  <span style=\"font-size:11px\">First name</span> </span>
                <input type=\"text\" value=\"Марко Андреас (Marco Andreas)\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">ДАТА РОЖДЕНИЯ <span style=\"font-size:11px\">Date of birth</span></span>
                <input type=\"text\" value=\"16.03.1972 пол муж\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">НОМЕР ПАСПОРТА  <span style=\"font-size:11px\">Passport No</span></span>
                <input type=\"text\" value=\"X2338202\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">ЦЕЛЬ ПОЕЗДКИ  <span
                    style=\"font-size:11px\">Purpose of the jurney</span></span>
                <input type=\"text\" value=\"ТУРИЗМ\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
                <span style=\"padding-right: 25px\">МАРШРУТ  <span style=\"font-size:11px\">Route</span></span>
                <input type=\"text\" value=\"Москва\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 20px; padding-top: 20px; padding-left: 20px\">
                <span style=\"padding-right: 25px\">РАЗМЕЩЕНИЕ  <span style=\"font-size:11px\">Accommodation</span></span>
                <input type=\"text\" value=\"Г-ца 'Столешников Бутик-отель'\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
            <div style=\"width: 100%; padding-bottom: 20px; padding-top: 20px; padding-left: 20px\">
                <span style=\"padding-right: 25px\">ВИД ОПЛАТЫ  <span
                        style=\"font-size:11px\">Method of payment</span></span>
                <input type=\"text\" value=\"ПОЛНОСТЬЮ ОПЛАЧЕНО\"
                       style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            </div>
        </form>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <p style=\"font-weight: bold\">ООО \"Эдельвейс Тур\"</p>
            <p>191186, г. Санкт-Петербург, Итальянская ул. д.17, литер А, пом.6Н Тел: +7 (812) 382-07-08</p>
        </div>
        <div style=\"width: 100%; margin-top: 40px\">
            <img src=\"./images/logo.png\" alt=\"logo\" style=\"width: 30%\">

        </div>
        <p style=\"margin-left: 25%\">Генеральный директор</p>
        <div style=\"width: 100%; position: relative\">
            <p><span style=\"float: left; margin-right: 20%\">М.П.</span>
                <span style=\"float: left; margin-right: 20%\">Воеводина Ю.А.</span>
                <span style=\"float: right; padding-right: 5px\">Дата: 19 January 2017</span>
            </p>

            <img src=\"./images/sing.png\" alt=\"sing\" style=\"width: 15%;position: absolute;bottom: 3%;left: 53%;\">

        </div>
    </div>
</div>

<div style=\"margin-bottom:20px; padding: 0; border:2px solid black;\">
    <div style=\"display:inline-block; width:50%; color:#000000; border-right: 2px solid black\">
        <div style=\"width: 100%; padding-bottom: 15px; padding-top: 10px; padding-left: 20px; position:relative\">
            <span style=\"padding-right: 25px\">ПОДТВЕРЖДЕНИЕ No</span>
            <input type=\"text\" value=\"1000\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            <h4 style=\"position: absolute; top:10px; right: 50px; font-size:25px; color: red\">COPY</h4>
        </div>
        <p style=\"padding-left: 20px; font-weight: bold\">о приеме иностранного туриста</p>
        <div style=\"width: 100%; padding-bottom: 5px;  padding-left: 20px\">
            <span style=\"padding-right: 25px\">КРАТНОСТЬ ВИЗЫ</span>
            <input type=\"text\" value=\"однократная\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">ГРАЖДАНСТВО</span>
            <input type=\"text\" value=\"Швейцария\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">ВЪЕЗД с</span>
            <input type=\"text\" value=\"17.02.2017 ВЫЕЗД по 20.02.2017\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">ФАМИЛИЯ</span>
            <input type=\"text\" value=\"Боккетти (Bocchetti)\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">ИМЯ, ОТЧЕСТВО</span>
            <input type=\"text\" value=\"Марко Андреас (Marco Andreas)\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">ДАТА РОЖДЕНИЯ</span>
            <input type=\"text\" value=\"16.03.1972 пол муж\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">НОМЕР ПАСПОРТА</span>
            <input type=\"text\" value=\"X2338202\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">ЦЕЛЬ ПОЕЗДКИ</span>
            <input type=\"text\" value=\"ТУРИЗМ\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">МАРШРУТ</span>
            <input type=\"text\" value=\"Москва\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 20px; padding-top: 20px; padding-left: 20px\">
            <span style=\"padding-right: 25px\">РАЗМЕЩЕНИЕ</span>
            <input type=\"text\" value=\"Г-ца 'Столешников Бутик-отель'\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <h4>Принимающая организация, ее адрес:</h4>
            <p>ООО \"Эдельвейс Тур\"</p>
            <p>191186, г. Санкт-Петербург, Итальянская ул. д.17, литер А, пом.6Н Тел: +7 (812) 382-07-08</p>
            <p>191186, St. Petersburg, Italyanskaya street 17 building A, office 6H Tel.: +7 (812) 382-07-08</p>
            <h4>No референса в МИД MBT 001772</h4>
            <h4 style=\"font-weight: normal\">ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ Индивидуальный тур</h4>
        </div>
        <div style=\"width: 100%\">
            <img src=\"./images/logo.png\" alt=\"logo\" style=\"width: 30%\">

        </div>
        <p style=\"margin-left: 25%\">Генеральный директор</p>
        <div style=\"width: 100%; position: relative\">
            <p><span style=\"float: left; margin-right: 20%\">М.П.</span>
                <span style=\"float: left; margin-right: 20%\">Воеводина Ю.А.</span>
                <span style=\"float: right; padding-right: 5px\">Дата: 19 January 2017</span>
            </p>

            <img src=\"./images/sing.png\" alt=\"sing\" style=\"width: 15%;position: absolute;bottom: 3%;left: 53%;\">

        </div>
    </div>
    <div style=\"display:inline-block; width:49%; color:#000000\">
        <div style=\"width: 100%; padding-bottom: 15px; padding-top: 10px; padding-left: 20px; position: relative;\">
            <span style=\"padding-right: 25px\">ВАУЧЕР No</span>
            <input type=\"text\" value=\"1000\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
            <h4 style=\"position: absolute; top:-30px; right: 50px; font-size:25px; color: red\">COPY</h4>
        </div>
        <p style=\"padding-left: 20px; font-weight: bold\">На оказание туристических услуг</p>
        <div style=\"width: 100%; padding-bottom: 5px;  padding-left: 20px\">
            <span style=\"padding-right: 25px\">КРАТНОСТЬ ВИЗЫ <span style=\"font-size:11px\">Type of visa</span> </span>
            <input type=\"text\" value=\"однократная\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">ГРАЖДАНСТВО <span style=\"font-size:11px\">Citizenship</span></span>
            <input type=\"text\" value=\"Швейцария\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">ВЪЕЗД с</span>
            <input type=\"text\" value=\"17.02.2017 ВЫЕЗД по 20.02.2017\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">пол <span style=\"font-size:11px\">Sex муж</span></span>
            <input type=\"text\" value=\"Боккетти (Bocchetti)\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">ФАМИЛИЯ <span style=\"font-size:11px\">Surname</span></span>
            <input type=\"text\" value=\"Боккетти (Bocchetti)\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">ИМЯ, ОТЧЕСТВО  <span style=\"font-size:11px\">First name</span> </span>
            <input type=\"text\" value=\"Марко Андреас (Marco Andreas)\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">ДАТА РОЖДЕНИЯ <span style=\"font-size:11px\">Date of birth</span></span>
            <input type=\"text\" value=\"16.03.1972 пол муж\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">НОМЕР ПАСПОРТА  <span style=\"font-size:11px\">Passport No</span></span>
            <input type=\"text\" value=\"X2338202\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">ЦЕЛЬ ПОЕЗДКИ  <span
                    style=\"font-size:11px\">Purpose of the jurney</span></span>
            <input type=\"text\" value=\"ТУРИЗМ\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <span style=\"padding-right: 25px\">МАРШРУТ  <span style=\"font-size:11px\">Route</span></span>
            <input type=\"text\" value=\"Москва\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 20px; padding-top: 20px; padding-left: 20px\">
            <span style=\"padding-right: 25px\">РАЗМЕЩЕНИЕ  <span style=\"font-size:11px\">Accommodation</span></span>
            <input type=\"text\" value=\"Г-ца 'Столешников Бутик-отель'\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 20px; padding-top: 20px; padding-left: 20px\">
            <span style=\"padding-right: 25px\">ВИД ОПЛАТЫ  <span style=\"font-size:11px\">Method of payment</span></span>
            <input type=\"text\" value=\"ПОЛНОСТЬЮ ОПЛАЧЕНО\"
                   style=\"border:none;background-color: transparent;float: right;padding-right: 35%;width: 206px;\">
        </div>
        <div style=\"width: 100%; padding-bottom: 5px; ; padding-left: 20px\">
            <p style=\"font-weight: bold\">ООО \"Эдельвейс Тур\"</p>
            <p>191186, г. Санкт-Петербург, Итальянская ул. д.17, литер А, пом.6Н Тел: +7 (812) 382-07-08</p>
        </div>
        <div style=\"width: 100%; margin-top: 40px\">
            <img src=\"./images/logo.png\" alt=\"logo\" style=\"width: 30%\">

        </div>
        <p style=\"margin-left: 25%\">Генеральный директор</p>
        <div style=\"width: 100%; position: relative\">
            <p><span style=\"float: left; margin-right: 20%\">М.П.</span>
                <span style=\"float: left; margin-right: 20%\">Воеводина Ю.А.</span>
                <span style=\"float: right; padding-right: 5px\">Дата: 19 January 2017</span>
            </p>

            <img src=\"./images/sing.png\" alt=\"sing\" style=\"width: 15%;position: absolute;bottom: 3%;left: 53%;\">

        </div>
    </div>
</div></body></head></html>";

        return $html;



    }
}

//
//ПОДТВЕРЖДЕНИЕ No о приеме иностранного туриста КРАТНОСТЬ ВИЗЫ ГРАЖДАНСТВО ВЪЕЗД с
//1000
//COPY
//ВАУЧЕР No 1000
//На оказание туристических услуг КРАТНОСТЬ ВИЗЫ Type of visa ГРАЖДАНСТВО Citizenship
//COPY
//ФАМИЛИЯ
//ИМЯ, ОТЧЕСТВО ДАТА РОЖДЕНИЯ НОМЕР ПАСПОРТА ЦЕЛЬ ПОЕЗДКИ МАРШРУТ
//Боккетти (Bocchetti)
//Last day of stay
//РАЗМЕЩЕНИЕ
//Принимающая организация, ее адрес:
//РАЗМЕЩЕНИЕ Accommodation ВИД ОПЛАТЫ Method of payment
//Г-ца "Столешников Бутик-отель" ПОЛНОСТЬЮ ОПЛАЧЕНО
//однократная Швейцария 17.02.2017
//ВЫЕЗД по
//ВЪЕЗД с
//пол Sex
//ФАМИЛИЯ Surname
//ИМЯ, ОТЧЕСТВО First name
//ДАТА РОЖДЕНИЯ Date of birth НОМЕР ПАСПОРТА Passport No ЦЕЛЬ ПОЕЗДКИ Purpose of the journey МАРШРУТ Route
//однократная Швейцария 17.02.2017 First day of entry
//ВЫЕЗД по
//20.02.2017
//20.02.2017 Марко Андреас (Marco Andreas)
//муж
//16.03.1972 X2338202
//пол муж
//Марко Андреас (Marco Andreas)
//ТУРИЗМ Москва
//X2338202
//Г-ца "Столешников Бутик-отель"
//ООО "Эдельвейс Тур"
//191186, г. Санкт-Петербург, Итальянская ул. д.17, литер А, пом.6Н Тел: +7 (812) 382-07-08 191186, St. Petersburg, Italyanskaya street 17 building A, office 6H Tel.: +7 (812) 382-07-08
//ООО "Эдельвейс Тур"
//No референса в МИД MBT 001772
//ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ Индивидуальный тур
//191186, г. Санкт-Петербург, Итальянская ул. д.17, литер А, пом.6Н Тел: +7 (812) 382-07-08
//Боккетти (Bocchetti)
//16.03.1972
//ТУРИ