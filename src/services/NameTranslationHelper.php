<?php

declare(strict_types = 1);

namespace App\Services;

use App\Repositories\LanguageTranslatorRepository;

/**
 * Class NameTranslationHelper
 * @package App\Services
 */
class NameTranslationHelper
{
    /**
     * @return string
     */
    public static function createNameTranslationToken() : string
    {
        return bin2hex(random_bytes(16));
    }

    /**
     * @param $newNameTranslation
     * @return bool
     * @throws \Exception
     */
    public static function emailLanguageTranslators($newNameTranslation) : bool
    {
        $translators = LanguageTranslatorRepository::fetchLanguageTranslators($newNameTranslation['origin_language_id']);
        foreach ($translators as $translator) {
            $message = self::buildMessageContentForTranslator(
                $newNameTranslation['origin_name'],
                'http://etraveldocs.de/translate/id/' . $translator['translator_id'] . '/token/' . $newNameTranslation['token']
            );

            EmailService::sendEmail($translator['email'], 'Name Translation Request', $message);
        }

        return true;
    }

    /**
     * @param $translationName
     * @param $url
     * @return string
     */
    public static function buildMessageContentForTranslator($translationName, $url) : string
    {
        $message = "<html><head><title>Translation for requested name. </title></head>";
        $message .= "<body><p>Hello, </p><p>There is a new name to be translated.</p><p>To translate $translationName click on the link above.</p>";
        $message .= "<form method=";
        $message .= "'post'";
        $message .= "action='" . $url . "'> ";
        $message .= "<input type=\"hidden\" name=\"token\" value=\"$url\"><button type=\"submit\">Go To Our Translation Page</button>";
        $message .= "</form></body></html>";

        return $message;
    }
}
