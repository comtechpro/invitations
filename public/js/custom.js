$(document).ready(function () {

    $('.apply-btn').click(function () {

        $('.applyDiv').addClass('active');

        $('.blue_line').css('display', 'block');
        $('.etd__form').css('display', 'block');

        $('html, body').animate({
            scrollTop: $('.etd__form').offset().top
        }, 1500);
    });

    $('#infoForm').validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            confirmemail: {
                required: true,
                email: true,
                equalTo: '#email'
            },
            citizenship: 'required',
            first_name: 'required',
            last_name: 'required',
            // date_of_birth: 'required',
            pass_number: 'required',
            pers_city: 'required',
            date_of_birth: {
                required: true,
                date: true
            },
            dateEntry: {
                required: true,
                date: true
            },
            dateExit: {
                required: true,
                date: true
            }

        },
        messages: {
            citizenship: "Please enter Citizenship",
            first_name: "Please enter First Name",
            last_name: "Please enter Last Name",
            // date_of_birth: "Please select your date of birth",
            pass_number: "Please enter passport number",
            pers_city: "Please enter a City name",
            date_of_birth: {
                required: 'Please enter a date',
                date: 'Enter a valid date'
            },
            dateEntry: {
                required: 'Please enter a date of entry',
                date: "Please enter a valid date"
            },
            dateExit: {
                required: "Please enter a exit date",
                date: "Please enter a valid date"
            }
        }
    });
    /* add input field*/
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $("#input_fields_wrap"); //Fields wrapper
    var add_button      = $("#add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="steps__f-grp"><label><i class="fa fa-info-circle"></i>Name of accommodation</label><a href="#" class="remove_field">Remove</a><input type="text" class="input--margin-r form-control"\n' +
                'aria-label="Name of accommodation"></div>'); //add input box
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    });
    /* add new person to form */

    var max_fieldsForm      = 3; //maximum input boxes allowed
    var wrapperForm         = $(".otherPeople"); //Fields wrapper
    var add_buttonForm      = $("#addPerson"); //Add button ID

    var x = 1; //initlal text box count
    $(add_buttonForm).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fieldsForm){ //max input box allowed
            x++; //text box increment
            $(wrapperForm).append('<div class="row">\n' +
                '    <div class="etd__form__step-1 col-md-12 col-lg-6">\n' +
                '        <h4 class="etd__form__step__heading">Step 1 / Your personal details</h4>\n' +
                '\n' +
                '        <div class="row">\n' +
                '            <div class="col-md-6 col-lg-12 col-xl-6">\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <label>Your email address</label>\n' +
                '                    <input class="form-control" type="email" name="email">\n' +
                '                </div>\n' +
                '\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <label>Citizenships</label>\n' +
                '                    <input class="form-control" type="text" name="citizenship">\n' +
                '                </div>\n' +
                '\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <label>First Name</label>\n' +
                '                    <input  class="form-control" type="text" name="first_name">\n' +
                '                </div>\n' +
                '\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <label>Last Name</label>\n' +
                '                    <input class="form-control" type="text" name="last_name">\n' +
                '                </div>\n' +
                '\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <label>Date of birth (dd/mm/yyyy)</label>\n' +
                '                    <input class="form-control" name="date_of_birth">\n' +
                '                    <!--<span class="input-group-btn">-->\n' +
                '                    <!--<button class="steps__f-grp__dtp-btn btn" type="button"><img src="img/icon/dt-picker.png" alt="Date-time picker icon"></button>-->\n' +
                '                    <!--</span>-->\n' +
                '\n' +
                '                </div>\n' +
                '\n' +
                '            </div>\n' +
                '\n' +
                '            <div class="col-md-6 col-lg-12 col-xl-6">\n' +
                '\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <label>Confirm your email address</label>\n' +
                '                    <input class="form-control" type="email" name="confirmemail">\n' +
                '                </div>\n' +
                '\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <p>Number of entries</p>\n' +
                '                    <div class="steps__f-grp__btn--wrapp">\n' +
                '                        <span class="steps__f-grp__btn btn--green">single</span>\n' +
                '                        <span class="steps__f-grp__btn">double</span>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <label>Middle Name</label>\n' +
                '                    <input class="form-control" type="text">\n' +
                '                </div>\n' +
                '\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <p>Gender</p>\n' +
                '                    <div class="steps__f-grp__btn--wrapp">\n' +
                '                        <span class="steps__f-grp__btn btn--green">male</span>\n' +
                '                        <span class="steps__f-grp__btn">female</span>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <label>Passport number</label>\n' +
                '                    <input class="form-control" type="number" name="pass_number">\n' +
                '                </div>\n' +
                '\n' +
                '            </div>\n' +
                '        </div>\n' +
                '    </div>\n' +
                '\n' +
                '    <div class="etd__form__step-2 col-md-12 col-lg-6">\n' +
                '        <div class="row">\n' +
                '            <div class="etd__form__step-2__form col-md-6 col-lg-12 col-xl-6">\n' +
                '                <h4 class="etd__form__step__heading">Step 2 / Trip info</h4>\n' +
                '\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <label>Date of entry (dd/mm/yyyy)</label>\n' +
                '                    <input type="text" class="form-control" name="dateEntry">\n' +
                '                </div>\n' +
                '\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <label>Date of exit (dd/mm/yyyy)</label>\n' +
                '                    <input type="text" class="form-control" name="dateExit">\n' +
                '                </div>\n' +
                '\n' +
                '                <!-- button class="steps__f-grp__btn-more" should generate another div class="steps__f-grp__accomo" -->\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <label><i class="fa fa-info-circle"></i>Name of accommodation</label>\n' +
                '                    <div class="steps__f-grp__accomo input-group">\n' +
                '                        <input type="text" class="input--margin-r form-control"\n' +
                '                               aria-label="Name of accommodation">\n' +
                '\n' +
                '                        <span class="input-group-btn">\n' +
                '\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button id=\'add_field_button\' class="steps__f-grp__btn-more btn btn-secondary " type="button">+</button>\n' +
                '\t\t\t\t\t\t\t\t\t\t\t\t\t</span>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <label>Neighboring countries to visit (first entry)</label>\n' +
                '\n' +
                '                    <div class="input-group">\n' +
                '                        <input type="text" class="form-control" placeholder="Select"\n' +
                '                               aria-label="Neighboring countries to visit, first entry">\n' +
                '\n' +
                '                        <div class="steps__f-grp__btn-grp input-group-btn">\n' +
                '                            <button type="button"\n' +
                '                                    class="steps__f-grp__dropdown-btn btn btn-secondary dropdown-toggle"\n' +
                '                                    data-toggle="dropdown" aria-haspopup="true"\n' +
                '                                    aria-expanded="false">\n' +
                '                            </button>\n' +
                '\n' +
                '                            <div class="steps__f-grp__dropdown-menu dropdown-menu dropdown-menu-right">\n' +
                '                                <a class="dropdown-item" href="#">Action</a>\n' +
                '\n' +
                '                                <a class="dropdown-item" href="#">Another action</a>\n' +
                '\n' +
                '                                <a class="dropdown-item" href="#">Something else here</a>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '\n' +
                '                <div class="steps__f-grp form-group">\n' +
                '                    <label>City</label>\n' +
                '                    <input class="form-control" type="text" name="pers_city">\n' +
                '                </div>\n' +
                '            </div>\n' +
                '\n' +
                '            <div class="etd__form__step-2__payment col-md-6 col-lg-12 col-xl-6">\n' +
                '                \n' +
                '            </div>\n' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>'); //add input box
        }
        var matched = $(".otherPeople").children().length + 1;
        console.log(matched);
        var value = 30 * matched;
        $(".payment__box__fee").text(value);
    });

    $(wrapperForm).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    });

    /* input change color radio btn */
    // $('.radio-input').click(function () {

    //     $('input:not(:checked)').parent().removeClass("btn--green");
    //     $('input:checked').parent().addClass("btn--green");

    //     console.log( $('input:checked').val() );
    // });


    // $('.label_fix').on('click', function() {

    //     $(this).addClass('btn--green');
    //     $(this).siblings().removeClass('btn--green');

    // });

    // Test
    // $("#submit").click(function(){      
    //     alert($('input[name=entries]:checked').val());
    //     alert($('input[name=gender]:checked').val());
    // });



});