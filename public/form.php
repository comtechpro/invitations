<?php
require '../vendor/autoload.php';
?>

<!doctype html>
<html lang="en">
<head>
    <title>E-Travel Docs</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
    <!-- Open Sans -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- Main CSS -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body class="etd--homepage etd-en">
<header class="etd__header">
    <div class="wrapper">
        <nav class="etd__header__nav navbar navbar-expand">
            <a class="etd__header__nav__logo d-none d-md-block navbar-brand" href="#"><img src="img/logo/logo.png" alt="Logo"></a>
            <a class="etd__header__nav__logo d-md-none navbar-brand" href="#"><img src="img/logo/logo-sm.png" alt="Logo"></a>

            <ul class="etd__header__nav__list navbar-nav">
                <li class="etd__header__nav__list__item nav-item">
                    <a href="#" target="_blank">faq</a>
                </li>

                <li class="etd__header__nav__list__item nav-item dropdown">
                    <button class="lang-btn nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="wrap">
                            <span class="lang-btn__short">en</span>
                            <span class="lang-btn__long">english<img src="img/flag/english.png" alt=""></span>
                        </div>
                    </button>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="lang-btn__short dropdown-item" href="#">de</a>
                        <a class="lang-btn__long dropdown-item" href="#">deutsch<img src="img/flag/german.png" alt="German flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">da</a>
                        <a class="lang-btn__long dropdown-item" href="#">deutch<img src="img/flag/danish.png" alt=""></a>

                        <a class="lang-btn__short dropdown-item" href="#">fi</a>
                        <a class="lang-btn__long dropdown-item" href="#">finnish<img src="img/flag/finnish.png" alt="Finnish flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">fr</a>
                        <a class="lang-btn__long dropdown-item" href="#">french<img src="img/flag/french.png" alt="French flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">it</a>
                        <a class="lang-btn__long dropdown-item" href="#">italian<img src="img/flag/italian.jpg" alt="Italian flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">nl</a>
                        <a class="lang-btn__long dropdown-item" href="#">dutch<img src="img/flag/dutch.jpg" alt="Deutch flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">no</a>
                        <a class="lang-btn__long dropdown-item" href="#">norwegian<img src="img/flag/norwey.png" alt="Norwegian flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">pl</a>
                        <a class="lang-btn__long dropdown-item" href="#">polish<img src="img/flag/polish.png" alt="Polosh flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">es</a>
                        <a class="lang-btn__long dropdown-item" href="#">spanish<img src="img/flag/spanish.png" alt="Spanish flag"></a>

                        <a class="lang-btn__short dropdown-item" href="#">sw</a>
                        <a class="lang-btn__long dropdown-item" href="#">swedish<img src="img/flag/swedish.png" alt="Swedish flag"></a>
                    </div>
                </li>
            </ul>
        </nav>
    </div><!-- /wrapper -->
</header><!-- /etd__header -->

<main class="etd__body">
    <form action="/stripe-store.php" method="POST">
        <script
            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
            data-key="pk_test_6pRNASCoBOKtIshFeQd4XMUh"
            data-amount="2000"
            data-name="Stripe.com"
            data-description="2 widgets"
            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
            data-locale="auto"
            data-zip-code="true">
        </script>
    </form>
    <!-- $Banner section -->
    <section class="etd__body__banner">
        <div class="wrapper">
            <div class="etd__body__banner__box">
                <h1>Need a Russia invitation? You are on the right place.</h1>

                <h3>Apply on our website today</h3>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="etd__body__banner__box__col col-full active">
                                <div class="etd__body__banner__box__col__top-triangle"></div>

                                <h2>Russia Tourist Invitation</h2>

                                <hr>

                                <div class="etd__body__banner__box__col__apply">
                                    <div class="wrapp">
                                        <span class="etd__body__banner__box__col__apply__cur">$</span>

                                        <span class="etd__body__banner__box__col__apply__fee">29</span>

                                        <span class="etd__body__banner__box__col__apply__fee small--font">99</span>
                                    </div>

                                    <button class="apply-btn">Apply now</button>
                                </div><!-- /etd__body__banner__box__col__apply -->
                            </div><!-- /etd__body__banner__box__col -->
                        </div><!-- /col-md-6 col-lg-6 -->

                        <div class="col-md-6 col-lg-6">
                            <div class="etd__body__banner__box__col">
                                <h2>Russia Business Invitation</h2>

                                <hr>

                                <div class="wrapp">
                                    <span>Coming soon</span>
                                </div>
                            </div><!-- /etd__body__banner__box__col -->
                        </div><!-- /col-md-6 col-lg-6 -->

                        <div class="col-md-6 col-lg-6">
                            <div class="etd__body__banner__box__col">
                                <h2>Belarus Tourist Invitation</h2>

                                <hr>

                                <div class="wrapp">
                                    <span>Coming soon</span>
                                </div>
                            </div><!-- /etd__body__banner__box__col -->
                        </div><!-- /col-md-6 col-lg-6 -->

                        <div class="col-md-6 col-lg-6">
                            <div class="etd__body__banner__box__col">
                                <h2>Vietnam Visa on arrival</h2>

                                <hr>

                                <div class="wrapp">
                                    <span>Coming soon</span>
                                </div>
                            </div><!-- /etd__body__banner__box__col -->
                        </div><!-- /col-md-6 col-lg-6 -->
                    </div>
                </div>
            </div><!-- /etd__body__banner__box -->
        </div><!-- /wrapper -->
    </section><!-- /etd__body__banner -->

    <!-- $Form section -->
    <section class="etd__form">
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">

                </div>
            </div>
        </div>
    </section>

    <!-- $FAQ section -->
    <section class="etd__body__faq">
        <div class="wrapper">
            <div class="etd__body__faq__container">
                <h2>FAQ's</h2>

                <p>To help, we’ve provided answers to the most frequently-asked questions that we’re posed. <span>However if you are still unsure or would like to know more, please do get in contact with us.</span></p>

                <hr>

                <div class="etd__body__faq__container__input input-group">
                    <input type="text" class="form-control" placeholder="Search for question" aria-label="Search for...">

                    <span class="input-group-btn">
					        	<button class="btn btn-secondary" type="button"><span class="fa fa-search" aria-hidden="true"></span></button>
					      	</span>
                </div><!-- /etd__body__faq__container__input input-group -->

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg etd__body__faq__container__q-box--wrapp">
                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can I pay for visa invitation online?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Yes, you can pay for visa invitation with your credit card Visa or Mastercard during the complition application form. You should make sure in advance that your credit card account has sufficient funds to pay that amount.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can I pay with someone else's credit card?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Yes, you can.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Is it possible to make a refund?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Unfortunately no. Payment for visa service is not refundable.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>How long will it take for me to receive my invitations after purchase?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>It’s automatic system. That’s why after the successful payment your invitation will be issued and sent to your e-mail. You will need just print it out and apply for a visa.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can you modify my invitation letter if the dates, name or some other information is incorrect?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>This information you should specify in the Consulate of Russian Federation. As we do not deal with any Consulates we are not able to inform you regarding this question.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->
                        </div><!-- /col-lg etd__body__faq__container__q-box--wrapp -->

                        <div class="col-lg etd__body__faq__container__q-box--wrapp">
                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can I pay for visa invitation online?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Yes, you can pay for visa invitation with your credit card Visa or Mastercard during the complition application form. You should make sure in advance that your credit card account has sufficient funds to pay that amount.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can I pay with someone else's credit card?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Yes, you can.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Is it possible to make a refund?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>Unfortunately no. Payment for visa service is not refundable.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>How long will it take for me to receive my invitations after purchase?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>It’s automatic system. That’s why after the successful payment your invitation will be issued and sent to your e-mail. You will need just print it out and apply for a visa.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->

                            <div class="etd__body__faq__container__q-box">
                                <div class="etd__body__faq__container__q-box__q">
                                    <span>q</span>
                                    <h4>Can you modify my invitation letter if the dates, name or some other information is incorrect?</h4>
                                </div>

                                <div class="etd__body__faq__container__q-box__a">
                                    <span>a</span>
                                    <p>This information you should specify in the Consulate of Russian Federation. As we do not deal with any Consulates we are not able to inform you regarding this question.</p>
                                </div>
                            </div><!-- /etd__body__faq__container__q-box -->
                        </div><!-- /col-lg etd__body__faq__container__q-box--wrapp -->
                    </div><!-- /row -->
                </div><!-- /container-fluid -->

                <button class="more-faq-btn">More FAQ</button>
            </div><!-- /etd__body__faq__container -->
        </div><!-- /wrapper -->
    </section><!-- /etd__body__faq -->
</main><!-- /etd__body -->

<footer>
    <div class="wrapper">
        <div class="f-header">
            <h2 class="f-header__heading">Subscribe to our awesome Newsletter</h2>

            <div class="wrapp">
                <input type="text" class="f-header__sign-up-input" placeholder="Enter your email address" aria-label="Enter your email address">

                <button type="button" class="sign-up-btn">Sign Up</button>
            </div><!-- /wrapp -->
        </div><!-- /f-header -->

        <div class="f-body">
            <div class="f-body__top-box">
                <img class="d-none d-lg-block" src="img/logo/logo-white.png" alt="Logo">
                <img class="d-lg-none" src="img/logo/logo-white-sm.png" alt="Logo">

                <div class="wrapp">
                    <a href="#">Russia Tourist invitation</a>

                    <a href="#">Russia Business Invitation</a>

                    <a href="#">Belarus Tourist Invitation</a>

                    <a href="#">Belarus Visa on arrival</a>
                </div><!-- /wrapp -->
            </div><!-- /f-body__top-box -->

            <div class="f-body__btm-box">
                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="img/icon/phone.png" alt="Phone icon">
                    <span>You can call us:</span>
                    <span><a href="tel:+41315281687">+ 41 315281687</a></span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="img/icon/email.png" alt="Email icon">
                    <span>You can emall us:</span>
                    <span><a href="mailto:hello@etraveldocs.com">hello@etraveldocs.com</a></span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__info">
                    <img class="d-none d-md-block" src="img/icon/customer-support.png" alt="Customer support icon">
                    <span>Customer support:</span>
                    <span>Monday/Friday - 09:00 - 17:00</span>
                </div><!-- /f-body__btm-box__info -->

                <div class="f-body__btm-box__soc-net">
                    <a href=""><img src="img/icon/facebook.png" alt="Facebook icon"></a>
                    <a href=""><img src="img/icon/twitter.png" alt="Twitter icon"></a>
                    <a href=""><img src="img/icon/linkedin.png" alt="LinkedIn icon"></a>
                    <a href=""><img src="img/icon/instagram.png" alt="Instagram icon"></a>
                </div><!-- /f-body__btm-box__soc-net -->
            </div><!-- /f-body__btm-box -->
        </div><!-- /f-body -->

        <div class="f-footer">
            All rights reserved bu Etraveldocs AG
        </div><!-- /f-footer -->
    </div><!-- /wrapper -->
</footer>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/jquery-3.2.1.slim.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>

