<?php

require '../vendor/autoload.php';

use App\Routing;
use App\Controllers\SetupController;
use App\Controllers\TranslatorController;
use App\Controllers\HomeController;
use App\Controllers\InvitationController;
use App\Controllers\HandlerController;
use App\Controllers\StripeController;
use App\Controllers\CronController;

// SETTING UP ROUTES
Routing::bootstrap(function ($ex) {
    header('Content-Type: text/html; charset=utf-8');
    $handlerController = new HandlerController();
    $handlerController->errorPage(HandlerController::PAGE_NOT_FOUND);
//    echo '404 - Page Not Found';
});


Route::get('/cron/invitations', function () {
    $cron = new CronController();
    $cron->emailTranslatedInvitations();
});

Route::get('/invitations/preview', function () {
    $invitation = new InvitationController();
    $invitation->preview();
});

Route::get('/translate/id/{id}/token/{token}', function ($id, $token) {
    $translator = new TranslatorController();
    $translator->translate($id, $token);
});

Route::post('lang/{lang}/translate/store', function ($lang) {
    $new =  new TranslatorController();
    return $new->store();
});

Route::get('migrate/token/{token}', function ($token) {
    $setup = new SetupController();
    $setup->migrate($token);
});

//--------------------------------------------------------------------------------------------------------------------------------------------

//InvitationController
Route::post('processing', function () {
    $new =  new InvitationController();
    return $new->store('en');
});

Route::post('/processing', function () {
    $new =  new InvitationController();
    return $new->store('en');
});

Route::post('/lang/{language}/processing', function ($language) {
    $new =  new InvitationController();
    return $new->store($language);
});
//Route::post('lang/processing', function () {
//    $new =  new InvitationController();
//    return $new->store();
//});


Route::get('invitation/{id}', function ($id) {
    $new =  new InvitationController();
    return $new->show($id);
})->where('id', '[0-9]+');


Route::post('/stripe/processing/invitation/{id}', function ($id) {
    $new =  new StripeController();
    return $new->processing($id);
});

Route::get('/stripe/processing', function () {
    $new =  new StripeController();
    return $new->processing();
});
//--------------------------------------------------------------------------------------------------------------------------------------------

// HomeController

Route::get('/', function () {
    $new =  new HomeController();
    return $new->index('en');
});

Route::get('/lang/{language}', function ($language) {
    $new =  new HomeController();
    return $new->index($language);
});


//
Route::get('home', function () {
    $new =  new HomeController();
    return $new->home();
});

//--------------------------------------------------------------------------------------------------------------------------------------------

//TranslatorController



Route::get('/translate/translator/{id}/token/{token}', function ($id, $token) {
    $translator = new TranslatorController();
    $translator->index($id, $token);
});





//--------------------------------------------------------------------------------------------------------------------------------------------
