<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/src/models/User.php';

$paths            = array(__DIR__ . '/models');
$isDevMode        = false;
$connectionParams = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'root',
    'password' => 'pass',
    'dbname'   => 'dbname',
);

$config = Setup::createConfiguration($isDevMode);
$driver = new AnnotationDriver(new AnnotationReader(), $paths);

// registering noop annotation autoloader - allow all annotations by default
AnnotationRegistry::registerLoader('class_exists');
$config->setMetadataDriverImpl($driver);

$em = EntityManager::create($connectionParams, $config);
//$user = $em->find('Users', 5); TODO test if returning the result
